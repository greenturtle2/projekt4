//funkcija za određivanje apija, npr za http://localhost:8080/signin staviti apiPath('signin')

function apiPath(path){             
    const api_url = 'https://backend-klinika7.onrender.com'; //url za backend (http://localhost:8080) (https://backend-klinika3.onrender.com)
    return api_url + "/" + path;
}

export default apiPath;
