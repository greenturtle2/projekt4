import React from "react";
import classes from "./servicesList.module.css";
import buttonclasses from "./registerForm.module.css"
import UserContext from "../../store/user-context";
import {useNavigate} from 'react-router-dom';
import apiPath from "../../global/api";
import{useContext} from "react";
import PatientServices from "./PatientServices";
import { useState, useEffect } from 'react';



function EvidentirajPacijenta(props){

    const userCtx = useContext(UserContext);
    const [isto, setIsto] = useState(false);
    const navigate = useNavigate();
    





          useEffect(() => {
           
            fetch(
              apiPath("evidencije/"+ props.id ),{
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json'
                }
        
            }
            )
              .then((response) => {
                return response.json();
              })
              .then((data) => {
                
        
                for (const key in data) {
                  const evidencija = {
                    id: key,
                    ...data[key]
                  };
                  const theBigDay = new Date().toLocaleDateString();
                  
                  const rr= new Date(evidencija.dan);         
                  console.log(theBigDay);
                  console.log(rr.toLocaleDateString());
               if (rr.toLocaleDateString() === theBigDay){
                  setIsto(true)}
                  console.log(rr.toLocaleDateString() === theBigDay);
                  
      
                }
            
               
              });
          },[props.id] );



    function EvidencijaHandler(data){
        
        fetch(apiPath("add/evidencija"), {
            method: 'POST',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
            }

        }).then((response)=>{
            return response.json();
            }).then(data=>{
              console.log(data);
              navigate("/evidencija", { replace: true });
               
            })
            ;}
   
       
   

        function submitHandler(event){          // funkcija za obradu submit-a  
            event.preventDefault();           // sprijeci podrazumjevano ponasanje (GET)
            const datum= new Date();
            const day = ("0" + datum.getDate()).slice(-2);
           const month = ("0" + (datum.getMonth() + 1)).slice(-2);
           const danas = datum.getFullYear()+ '-' + month + '-' + day;
           
           
            
            
            const registerData = {
                
                dan:danas,
               
                bolesnik_id: props.id,
                doktor_id: userCtx.id
    
              };
    
              EvidencijaHandler(registerData);
              window.location.reload();
              

    }
    if (!isto){return (
        
        <li>
          <div className={classes.wrapper} >
          <div className={classes.section} >
          <h3>{props.firstname} {props.lastname} </h3>
            <p>MBO: {props.mbo}</p>
            <p>Dijagnoza: {props.diagnosis}</p>
            <p><b>Usluge:</b> <PatientServices id={props.id}/> </p>
            <p>Dodatno Zdravstveno: {props.dodatno}</p>
            <p>Kontakt: {props.email}, {props.phone}</p>
          <button    onClick ={submitHandler} className={buttonclasses.btn}>Evidentiraj</button>
          
          </div>
          
          </div>
          
        </li>
        
    );}
    return;
}

export default EvidentirajPacijenta;