import React from 'react';
import classes from  "./ShowSuccess.module.css";
import CardWrapper from '../UI/cardWrapper';
import { useNavigate } from "react-router-dom";
import {useState} from "react";
import apiPath from '../../global/api';

function GeneratePdf(props) {

    

    const [next, setNext] = useState(false);
    const navigate = useNavigate();

    function handleGenerate(){
        fetch(apiPath("pdf/generiraj/" + props.mbo), {
            method: 'GET',
            headers: {
                'Content-Type': 'application/pdf'
            }
    
        }).then((response)=>{
            response.blob().then(blob => showInOtherTab(blob));
            }).then(data=>{
                navigate("/");
            });

    }
    function showInOtherTab(blob) {
        const url = window.URL.createObjectURL(blob);
        window.open(url);
      }



    return(
      <div className = {classes.container}  >
        <div className = {classes.content}>
          <CardWrapper>
          <button className={classes.btn} onClick={handleGenerate}>GENERIRAJ PDF</button>
 
      </CardWrapper>

        </div>

      </div>
    )       
}
export default GeneratePdf;