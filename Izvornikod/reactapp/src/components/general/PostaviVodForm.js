import classes from  "./registerForm.module.css";
import React from "react";
import LijecnikDropDown from './LijecnikDropDown';
import { useRef } from 'react';
import apiPath from "../../global/api";
import { useNavigate } from "react-router-dom";


function PostaviVodForm(){
   const navigate = useNavigate();
    const days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
    //const now = new Date().getDate();
    var curr = new Date();
    const day = days[curr.getDay()];
   
             //reference na dijelove (input) forme
    const doktorVoditeljPon = useRef();
    const doktorVoditeljUto = useRef();
    const doktorVoditeljSri = useRef();
    const doktorVoditeljČet = useRef();
    const doktorVoditeljPet = useRef();
    
    function PostaviHandler(data){
         
        fetch(apiPath("add/dnevnivoditelj"), {
            method: 'POST',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
            }

        }).then((response)=>{
            //return response.json();
            }).then(data=>{
              console.log(data);
              
               
            })
            ;}
           /*  function padTo2Digits(num){
                return num.toString().padStart(2,'0');
            }
            function formatDate(date){
                return [
                    date.getFullYear(),
                    padTo2Digits(date.getMonth()+1),
                    padTo2Digits(date.getDate())
                ].join('-');
            } */

            function SubmitHandler(event){          // funkcija za obradu submit-a  
                event.preventDefault();           // sprijeci podrazumjevano ponasanje (GET)
               // const datum= new Date();
                const DoktorVoditeljPon = doktorVoditeljPon.current.value;
                const DoktorVoditeljUto = doktorVoditeljUto.current.value;
                const DoktorVoditeljSri = doktorVoditeljSri.current.value;
                const DoktorVoditeljČet = doktorVoditeljČet.current.value;
                const DoktorVoditeljPet = doktorVoditeljPet.current.value;
               /*  var uto;
                var sri;
                var čet;
                var pet; */
                
                //datum.setHours(0,0,0,0);
                
                
                var broj1 = curr.getDate() + 1;
                var utorak = new Date();
                utorak.setDate(broj1);

                var broj2 = curr.getDate() + 2;
                var srijeda = new Date();
                srijeda.setDate(broj2);

                var broj3 = curr.getDate() + 3;
                var četvrtak = new Date();
                četvrtak.setDate(broj3);

                var broj4= curr.getDate() + 4;
                var petak = new Date();
                petak.setDate(broj4);

                
                
                
                const registerDataP = {
                    danVodenja: curr,
                    doktor_id: DoktorVoditeljPon};

                const registerDataU = {
                    danVodenja: utorak ,
                    doktor_id: DoktorVoditeljUto};

                const registerDataS = {
                    danVodenja: srijeda  ,
                    doktor_id: DoktorVoditeljSri};

                const registerDataČ = {
                    danVodenja:četvrtak ,
                    doktor_id: DoktorVoditeljČet};

                const registerDataPt = {
                    danVodenja: petak ,
                    doktor_id: DoktorVoditeljPet};
                        
                    
                 
                PostaviHandler(registerDataP);

               
                PostaviHandler(registerDataU);

                
                PostaviHandler(registerDataS);

               
                PostaviHandler(registerDataČ);

               
                PostaviHandler(registerDataPt);
                
                navigate("/");
    
        }  
       
        if ( day === 'Thursday'){ // 'Monday'
        return(
            
        
            
            <form className={classes.form} onSubmit={SubmitHandler}>
            <div className={classes.formBody}>
              
              <div>

              <label className={classes.label} for="voditelj">Dnevni voditelj za ponedjeljak:  <LijecnikDropDown reference={doktorVoditeljPon}/> </label>
              </div>
              <div>
              <label className={classes.label} for="voditelj">Dnevni voditelj za utorak:  <LijecnikDropDown reference={doktorVoditeljUto}/> </label>
              </div>
              <div>
              <label className={classes.label} for="voditelj">Dnevni voditelj za srijedu:  <LijecnikDropDown reference={doktorVoditeljSri}/> </label>
              </div>
              <div>
              <label className={classes.label} for="voditelj">Dnevni voditelj za četvrtak:  <LijecnikDropDown reference={doktorVoditeljČet}/> </label>
              </div>
              <div>
              <label className={classes.label} for="voditelj">Dnevni voditelj za petak:  <LijecnikDropDown reference={doktorVoditeljPet}/> </label>
              </div>

              <div >
              <button type="submit"  className={classes.btn}>Submit </button>
          </div>
              
              </div>
              
              </form>
            
              
            
            
        ) } else 
        return (
            <h1> Voditelji postavljeni za ovaj tjedan! </h1>
        );

    
    }
    
    export default PostaviVodForm;
