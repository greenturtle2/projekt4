import {useRef } from 'react';
import React from 'react';
import classes from  "./registerForm.module.css";
import { useLocation } from "react-router-dom";
import PatientServiceDropDown from './PatientServiceDropDown';
import ServiceDropDown from './ServiceDropDown';
import * as moment from 'moment';



function AddAppointmentForm(props) {
    const location = useLocation();
    const bolesnik_id = location.state.bolesnik_id;
    const startDateInputRef = useRef();
    const uslugaInputRef = useRef();

    
  function checkDate(event){
    event.preventDefault();
    const date2 = new Date(startDateInputRef.current.value);
    if(date2.getHours() >= 9 && date2.getHours() <= 17 && date2.getDay() >= 1 && date2.getDay() <= 5){
      submitHandler(event);
        
    }else{
        alert('odabrani termin mora biti RADNIM DANOM IZMEĐU 9 i 17 SATI!');
    }
  }      
    function submitHandler(event){          // funkcija za obradu submit-a  
        event.preventDefault();             // sprijeci podrazumjevano ponasanje (GET)
      //  const re = /T|:/g;
        var  enteredStartDate=startDateInputRef.current.value.replace('T', ' ');
        var dateTime = enteredStartDate.split(' ');
        const dan = dateTime[0];
        const vrijeme = dateTime[1];
        var date = new Date(dan);
        const novidate = moment(date).format('DD-MM-YYYY');
      
        
        const enteredUsluga = uslugaInputRef.current.value;
     //   const enteredEndDate = endDateInputRef.current.value.replace(re, '-') + "-00";

      
      
        const appointmentData = {
            bolesnik_id: bolesnik_id,
            dan: novidate,
            vrijeme: vrijeme,
           // pocetakTermina: enteredStartDate,
          //  usluga_trajanje: '90',
           // uslugaId: '1'
            usluga: enteredUsluga
        
          };

         // console.log(appointmentData);
          props.onRegister(appointmentData);       //funkcija koja definira sto ce se dogoditi s unesenim podacima se definira u pozivajucoj komponenti
    }

    return(
      <form className={classes.form} onSubmit={checkDate}>
          <div className={classes.formBody}>
             
              <div className="pocetak_termina">
                  <label className={classes.label} for="pocetak_termina">Početak termina: </label>
                  <input  required type="datetime-local"  ref={startDateInputRef}/>
              </div>

              {/* <div className="kraj_termina">
                  <label className={classes.label} for="kraj_termina">Kraj termina: </label>
                  <input  required type="datetime-local" ref={endDateInputRef}/>
                  </div> */}
            <label className={classes.label} for="mojeusluge">Usluga: </label>  <PatientServiceDropDown patientId = {bolesnik_id} reference={uslugaInputRef}/>   
             

        
          </div>

          <div >
              <button type="submit" className={classes.btn}>Postavi</button>
          </div>

      </form>      
    )       
}
export default AddAppointmentForm;
