import classes from "./servicesList.module.css";
import classesB from "./registerForm.module.css"
import React from "react";
import { useContext } from "react";

import UserContext from "../../store/user-context";
import { useState, useEffect } from 'react';
import apiPath from "../../global/api";
import ListaRasporeda from "./ListaRasporeda"
import ServiceDropDown from "./ServiceDropDown";
import { useRef } from "react";
function PregledStanjaForm(){
    const [isLoading, setIsLoading] = useState(true);
    const [loadedRaspored, setLoadedRaspored] = useState([]);
    const userCtx = useContext(UserContext);
 

    function checkWeek(datet){
        
        const todayObj = new Date();
        const todayDate = todayObj.getDate();
        const todayDay = todayObj.getDay();
        datet = datet.substring(0,10);
        const date = new Date(datet);
        const firstDayOfWeek = new Date(todayObj.setDate(todayDate - todayDay));
        const lastDayOfWeek = new Date(firstDayOfWeek);
        lastDayOfWeek.setDate(lastDayOfWeek.getDate() + 6);

        return date >= firstDayOfWeek && date <= lastDayOfWeek;
    }
   
     useEffect(() => {
      setIsLoading(true);
      fetch(
        apiPath("termini"),{
          method: 'GET',
          headers: {
              'Content-Type': 'application/json'
          }
  
      }
      )
        .then((response) => {
          return response.json();
        })
        .then((data) => {
          const termini = [];
  
          for (const key in data) {
            const termin = {
              id: key,
              ...data[key]
            };
           
           
            var datet = termin.pocetakTermina;
            if(checkWeek(datet)){
                termini.push(termin);
            };
                
            
          }
          setIsLoading(false);
          setLoadedRaspored(termini);
        });
    },[userCtx.id ] );
if(isLoading){
    return(
    <div className={classes.listContainer}>
        <p>Dohvaćanje pregleda...</p>
    </div>
);
}
    return(
    <div className={classes.listContainer}>
       <ListaRasporeda termini={loadedRaspored} />
    </div>
);


}
export default PregledStanjaForm;