import EvidentirajPacijenta from "./EvidentirajPacijenta";
import classes from "./servicesList.module.css";


function ListaPacijenataE(props) {
  return (
    <ul className={classes.list}>
      {props.patients.map((patient) => (
        <EvidentirajPacijenta
          key={patient.id}
          id={patient.id}
          diagnosis = {patient.dijagnoza}
          dodatno = {patient.dodatno_zdravstveno}
          email = {patient.email}
          firstname={patient.ime}
          lastname = {patient.prezime}
          mbo = {patient.mbo}
          doctor = {patient.moj_lijecnik}
          service  = {patient.moje_usluge}
          phone = {patient.telefon}

        />
      ))}
    </ul>
  );
}

export default ListaPacijenataE;