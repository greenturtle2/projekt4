import React from "react";
import classes from "./servicesList.module.css";
import Elektro from "./images/Elektro.jpg";
import MedMas from "./images/MedMas.jpg";
import Krioterap from "./images/Krioterap.jpg";
import Kinesio from "./images/Kinesio.jpg";
import Schroth from "./images/Schroth.jpg";

function ServicesList() {
  return (
  
      <div className={classes.service}>
         <div className={classes.bg}/>
         <div className={classes.bgfadetopmoving}/>
         <div className={classes.bgfadebottom}/>

        <h1> Zdravlje <b>nije sve</b>, ali bez zdravlja sve drugo je <b>ništa</b>.</h1>

        <div className={classes.wrapper}>
          <div className={classes.section}>
            <img src={Elektro} alt="ELEKTROTERAPIJA"></img>
            <h2>Elektroterapija </h2>
            <p>
              Elektroterapija predstavlja primjenu električne struje radi
              postizanja terapijskog učinka. Elektroterapijom postižemo
              toplinski učinak koji utječe na proširenje krvnih žila, postižemo
              analgetski učinak stimuliranjem živčanih vlakana zaduženih za
              prijenos boli, djelujemo na povećanje mišićne mase te stimuliramo
              oštećene živce.
            </p>
          </div>

          <div className={classes.section}>
            <img src={MedMas} alt="MEDICINSKA MASAŽA"></img>
            <h2>Medicinska masaža </h2>
            <p>
              Medicinska masaža je metoda fizikalne terapije koja se služi nizom
              sustavnih pokreta na tijelu s ciljem manipulacije tjelesnim
              tkivima (kožom, potkožjem, mišićima i mišićnom fascijom). Izvodi
              se rukama u svrhu izravnog ili neizravnog djelovanja na mišićni i
              živčani sustav te cirkulaciju krvi i limfe.
            </p>
          </div>

          <div className={classes.section}>
            <img src={Krioterap} alt="KRIOTERAPIJA"></img>
            <h2>Krioterapija </h2>
            <p>
              Krioterapija je liječenje ledom koje se provodi primjenom leda
              čijom hladnoćom želimo postići bolju prokrvljenost i duboku
              vazodilataciju i istovremeno blokirati aferentne živce smanjujući
              osjet boli, pogotovo kod svježih ozljeda. Krioterapija se koristi
              kod akutnih bolnih stanja i nakon sportskih ozljeda te kao
              predradnja vježbama da bi smanjili bolnost tijekom vježbanja.
            </p>
          </div>

          <div className={classes.section}>
            <img src={Kinesio} alt="KINESIO TAPING"></img>
            <h2>Kinesio Taping </h2>
            <p>
              Kinesio taping je tehnika koja potiče prirodan proces
              ozdravljenja. Za svoju primjenu, Kinesio Taping, koristi
              specijalnu traku koja imitira svojstva kože, te se ovisno o
              terapijskom cilju primjenjuje u različitim stupnjevima napetosti.
              Ova metoda koristi se kod raznih ozljeda mišića i zglobova,
              hematoma te mnogih bolnih stanja.
            </p>
          </div>

          <div className={classes.section}>
            <img src={Schroth} alt="SCHROTH METODA"></img>
            <h2>Schroth metoda</h2>
            <p>
              Schroth metoda je sistem izvođenja vježbi koji se primenjuje u
              fizoterapiji pri liječenju skolioza, povećanje vitalnog kapaciteta
              pluća, poboljšanje posture, kao i osposobljavanje pacijenta da
              dugotrajno održi postignutu korekciju te time izbjegne kiruršku
              intervenciju.
            </p>
          </div>
        </div>
      </div>
  
  );
}

export default ServicesList;
