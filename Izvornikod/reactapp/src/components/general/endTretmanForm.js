import React from 'react';
import { useRef } from 'react';
import classes from  "./registerForm.module.css";
import {useNavigate } from "react-router-dom";
import { useLocation } from "react-router-dom";

function EndTretmanForm(props){
    const location = useLocation();
    const navigate = useNavigate();
    const bolesnik_id = location.state.bolesnik_id;
    const bolesnik_mbo = location.state.bolesnik_mbo;
    const napredakInputRef = useRef();
    const zadovoljanInpuRef = useRef();
    const izjavaInputRef = useRef();
    const descriptionInputRef = useRef();
    function addServiceHandler(event){          
        event.preventDefault(); 
        const enteredNapredak = napredakInputRef.current.value;
        const enteredZadovoljan = zadovoljanInpuRef.current.value;
        const enteredIzjava = izjavaInputRef.current.value;
        const enteredDesc = descriptionInputRef.current.value;
        console.log(bolesnik_id);
        const serviceData = {
            bolesnik: bolesnik_id,
            napredak: enteredNapredak,
            zadovoljan: enteredZadovoljan,
            izjava: enteredIzjava,
            zapazanje: enteredDesc,
            
        };
        props.onRegister(serviceData, bolesnik_mbo);
       // navigate("/");
    }


    return(
        <form className={classes.form} onSubmit={addServiceHandler}>
            <div className={classes.formBody}>
                <div className="napredak">
                    <label className="form__label" for="napredak">Napredak: </label>
                    <input required className="form__input" type="text" id="napredak" placeholder="npr.Značajan" ref={napredakInputRef}/>
                </div>
            
                <div className="zadovoljan">
                    <label className={classes.label} for="">Zadovoljan tretmanom: </label>
                    <input  required type="text" name="" id="zadovoljan"  className="form__input" placeholder="Da/Ne" ref={zadovoljanInpuRef}/>
                </div>
                <div className="izjava">
                    <label className={classes.label} for="izjava">Izjava bolesnika: </label>
                    <textarea  required name="" id="izjava"  className="form__input" placeholder="Izjava bolesnika..." ref={izjavaInputRef}/>
                </div>
                <div className="opis">
                    <label className={classes.label} for="opis">Zapažanja: </label>
                    <textarea  required name="" id="opis"  className="form__input" placeholder="Zapažanja tijekom tretmana bolesnika" ref={descriptionInputRef}/>
                </div>
          
            </div>
  
            <div >
                <button type="submit" className={classes.btn}>Završi </button>
            </div>
  
        </form>      
      )  
}
export default EndTretmanForm;