import React from "react";
import { useState, useEffect} from 'react';
import { useRef } from 'react';
import apiPath from "../../global/api";


function LijecnikDropDown(props) {
    const [isLoading, setIsLoading] = useState(true);
    const [items, setItems] = useState([{ label: "Loading ...", value: "" }]);
    const [value, setValue] = useState();

    

    var lijecnikInputRef= useRef();
    lijecnikInputRef = props.reference;

    useEffect(() => {
        setIsLoading(true);
  fetch(
          apiPath("djelatnici"),{
            method: 'GET',
            headers: {
                'Content-Type': 'application/json'
            }
    
        }
        )
          .then((response) => {
            return response.json();
          })
          .then((data) => {
            const doctors = [];
    
            for (const key in data) {
              const doctor = {
                id: key,
                ...data[key]
              };
              console.log(doctor);
              doctors.push(doctor);
            }
          
          
            setItems(doctors.map((doctor) => ({ label: doctor.ime + " " + doctor.prezime, value: doctor.id})));
            
            
            setIsLoading(false);
          
        });
  
  
      }, []);

      
       return(
        <select disabled={isLoading} ref={lijecnikInputRef}
        value={value}
      onChange={e => setValue(e.currentTarget.value)} >
      {items.map(item => (
        <option
          key={item.value}
          value={item.value}
        >
          {item.label}
        </option>
      ))}
    </select>
  );
      }    

     
      export default LijecnikDropDown;
   