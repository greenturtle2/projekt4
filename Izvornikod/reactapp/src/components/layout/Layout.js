import Navigation from './Navigation';
import classes from './Layout.module.css';
import Greeting from './Greeting';

function Layout(props) {
  return (
    <div>
      <Navigation />
      <main className={classes.main}>{props.children}</main>
      <Greeting />
    </div>
  );
}

export default Layout;