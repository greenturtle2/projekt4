import AddAppointmentForm from '../components/general/addAppointmentForm';
import {useNavigate} from 'react-router-dom';
import apiPath from "../global/api";
import UserContext from "../store/user-context";
import {useContext, useState} from 'react';



function AddAppointment(props) {
  const navigate = useNavigate();

    const userCtx = useContext(UserContext);
    
    function addAppt(data){
      fetch(apiPath("add/termintest"), {
        method: 'POST',
        body: JSON.stringify(data),
        headers: {
            'Content-Type': 'application/json'
        }

    }).then((response)=>{
        return response.text();
        }).then(data=>{
            console.log(data.toString());
            alert('Termin uspješno dodan!');
           navigate("/patients");
        })
        ;
    }
    function addAppointmentHandler(data){
      const first = data;
      fetch(apiPath("provjeri/termin"), {
            method: 'POST',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
            }

        }).then((response)=>{
            return response.text();
            }).then(data=>{
                console.log(data.toString());
               if(data.toString() === 'ne'){
                addAppt(first);
               } else {
                alert('Odabrani termin je nedostupan!');
               }
            })
            ;
    }
    return (
      <section>
        
        {userCtx.role === 'ROLE_MEDIC' ? 
       
        <AddAppointmentForm onRegister = {addAppointmentHandler}/>
        : <h1> Neovlašten pristup! </h1>}

        
      </section>
    );
  }
  
  export default AddAppointment;
