import RasporedForm from "../components/general/RasporedForm.js";
import { useContext } from "react";
import UserContext from "../store/user-context";

import React from "react";
function Raspored() {

    const userCtx = useContext(UserContext);
    
      
  
     
  
      return (
        <section>
          {userCtx.role === 'ROLE_BOLESNIK' ? 
          <RasporedForm />
          : <h1> Neovlašten pristup! </h1>}
          
          
        </section>
      );
    }
    
    export default Raspored;