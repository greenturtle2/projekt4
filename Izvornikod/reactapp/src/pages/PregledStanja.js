import { useContext } from "react";
import UserContext from "../store/user-context";

import React from "react";
import PregledStanjaForm from "../components/general/pregledStanjaForm.js";
function PregledStanja() {

    const userCtx = useContext(UserContext);
    
      
  
     
  
      return (
        <section>
          {userCtx.role === 'ROLE_VODITELJ' ? 
          <PregledStanjaForm />
          : <h1> Neovlašten pristup! </h1>}
          
          
        </section>
      );
    }
    
    export default PregledStanja;