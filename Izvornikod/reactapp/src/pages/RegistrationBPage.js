import RegistrationBForm from "../components/general/registerBForm";
import { useState, useContext } from "react";
import AddSuccessPage from "./AddSuccessPage";
import apiPath from "../global/api";
import UserContext from "../store/user-context";
import React from "react";

function RegistrationBPage() {

  const userCtx = useContext(UserContext);
  
    const [isRegistered, setIsRegistered] = useState(false);
    const [id, setId] = useState("false");

    

    function registrationHandler(mydata){
      
        fetch(apiPath("signup/b"), {
            method: 'POST',
            body: JSON.stringify(mydata),
            headers: {
                'Content-Type': 'application/json'
            }

        }).then((response)=>{
            return response.json();
            }).then(data=>{

                setIsRegistered(data.message);
                setId(data.id);

                const mailData = {
                  primatelj: mydata.email,
                  predmet:"LOZINKA ZA PRIJAVU",
                  poruka: data.message
                };

                if(!data.message.includes("Error")){
                  
                
                fetch(apiPath("send/email"), {
                  method: 'POST',
                  body: JSON.stringify(mailData),
                  headers: {
                      'Content-Type': 'application/json'
                  }});
                
                }
                
                
            })
            ;
            
    }

    return (
      <section>
        {userCtx.role === 'ROLE_MEDIC' ? 
        <RegistrationBForm onRegister = {registrationHandler}/>
        : <h1> Neovlašten pristup! </h1>}
        
        {isRegistered ? <AddSuccessPage message={isRegistered} id = {id}/> : null}
      </section>
    );
  }
  
  export default RegistrationBPage;
