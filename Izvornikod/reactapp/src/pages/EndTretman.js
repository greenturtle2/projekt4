import { useContext } from "react";
import UserContext from "../store/user-context";
import React from "react";
import EndTretmanForm from "../components/general/endTretmanForm";
import apiPath from "../global/api";
import GeneratePdfPage from './GeneratePdfPage'
import { useState } from "react";



function EndTretman() {
    const[isSent, setSent]= useState(false);
    const [mbo, setMBO] = useState("false");
    const userCtx = useContext(UserContext);
    function registrationHandler(data, mbo){

        fetch(apiPath("add/zavrsetak"), {
            method: 'POST',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
            }
    
        }).then((response)=>{
            return response.json();
            }).then(data=>{
                console.log(data);
                setMBO(mbo);
                setSent(true);
            });
        
    }
    return (
        <section>
          
          {userCtx.role === 'ROLE_MEDIC' ? 
         
          <EndTretmanForm onRegister = {registrationHandler}/>
          : <h1> Neovlašten pristup! </h1>}
  
   {isSent ? <GeneratePdfPage mbo={mbo}/> : null}
        </section>
      );
}
export default EndTretman;