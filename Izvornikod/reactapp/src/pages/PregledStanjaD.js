import { useContext } from "react";
import UserContext from "../store/user-context";

import React from "react";
import PregledStanjaDForm from "../components/general/pregledStanjaDForm.js";
function PregledStanjaD() {

    const userCtx = useContext(UserContext);
    
      
  
     
  
      return (
        <section>
          {userCtx.role === 'ROLE_MEDIC' ? 
          <PregledStanjaDForm />
          : <h1> Neovlašten pristup! </h1>}
          
          
        </section>
      );
    }
    
    export default PregledStanjaD;