import RegistrationForm from "../components/general/registerForm";
import { useState, useContext } from "react";
import RegSuccessPage from "./RegSuccessPage";
import UserContext from "../store/user-context";
import React from "react";
import apiPath from "../global/api";

function RegistrationPage() {

    const userCtx = useContext(UserContext);

    

    const [isRegistered, setIsRegistered] = useState(false);

    function registrationHandler(mydata){

        fetch(apiPath("signup/d"), {
            method: 'POST',
            body: JSON.stringify(mydata),
            headers: {
                'Content-Type': 'application/json'
            }

        }).then((response)=>{
            return response.json();
            }).then(data=>{
                setIsRegistered(data.message);

                const mailData = {
                  primatelj: mydata.email,
                  predmet:"LOZINKA ZA PRIJAVU",
                  poruka: data.message
                };
                console.log(mailData);

                
                  
                if(data.message.includes("uspjesno"))  {
                  fetch(apiPath("send/email"), {
                    method: 'POST',
                    body: JSON.stringify(mailData),
                    headers: {
                        'Content-Type': 'application/json'
                    }});
                }
                

                
                
            })
            ;
    }

    return (
      <section>
        {userCtx.role === 'ROLE_ADMIN' ? 
        <RegistrationForm onRegister = {registrationHandler}/>
        : <h1> Neovlašten pristup! </h1>}

        {isRegistered ? <RegSuccessPage message={isRegistered} /> : null}

      </section>
    );
  }
  
  export default RegistrationPage;
