import PostaviVodForm from "../components/general/PostaviVodForm.js";
import { useContext } from "react";
import UserContext from "../store/user-context";

import React from "react";
function PostaviVod() {

    const userCtx = useContext(UserContext);
    
      
  
     
  
      return (
        <section>
          {userCtx.role === 'ROLE_VODITELJ' ? 
          <PostaviVodForm />
          : <h1> Neovlašten pristup! </h1>}
          
          
        </section>
      );
    }
    
    export default PostaviVod;