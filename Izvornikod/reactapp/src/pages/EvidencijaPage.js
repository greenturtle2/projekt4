import EvidencijaForm from "../components/general/EvidencijaForm";
import { useContext } from "react";
import UserContext from "../store/user-context";
import { useState, useEffect } from 'react';
import apiPath from "../global/api";




import React from "react";

function EvidencijaPage() {

    const userCtx = useContext(UserContext);
    const [isLoading, setIsLoading] = useState(true);
    const [voditelj, setvoditelj] = useState("false");
    
    

    
    useEffect(() => {
      setIsLoading(true);
      const today = new Date();
      const day = ("0" + today.getDate()).slice(-2);
      const month = ("0" + (today.getMonth() + 1)).slice(-2);
      const danas = today.getFullYear() + '-' + month + '-' + day; 
      console.log(danas);
      fetch(
        apiPath("dnevnivoditelj/" + danas ),{
          method: 'GET',
          headers: {
              'Content-Type': 'application/json'
          }
  
      }
      )
        .then((response) => {
          return response.json();
        })
        .then((data) => {
          
          setvoditelj(data.toString());
  
          setIsLoading(false);
         
          
        });
    },[] );
if(isLoading){  return(
  
     <p>Dohvaćanje dnevnog voditelja ...</p>
  
);}
    
      
  
     
  
      return (
        <section>
          {userCtx.role === 'ROLE_MEDIC' && userCtx.id === voditelj  ? 
          <EvidencijaForm />
          : <h1> Neovlašten pristup! </h1>}
          
          
        </section>
      );
    }
    
    export default EvidencijaPage;
  

