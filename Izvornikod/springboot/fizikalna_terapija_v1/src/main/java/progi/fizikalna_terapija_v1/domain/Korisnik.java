package progi.fizikalna_terapija_v1.domain;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "Korisnik",
        uniqueConstraints = {
            @UniqueConstraint(columnNames = "username")
        })
public class Korisnik {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotBlank
    private String username;

    @NotBlank
    private String password;

    private String ime;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "Korisnici_uloge",
                joinColumns = @JoinColumn(name = "korisnik_id"),
                inverseJoinColumns = @JoinColumn(name = "uloga_id"))
    private Set<Uloga> uloge = new HashSet<>();


    public Korisnik(){}

    public Korisnik(String username, String password){
        this.username = username;
        this.password = password;
    }

    public Korisnik(String username, String password, String ime) {
        this.username = username;
        this.password = password;
        this.ime = ime;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Set<Uloga> getUloge() {
        return uloge;
    }

    public void setUloge(Set<Uloga> uloge) {
        this.uloge = uloge;
    }

    public String getIme() {
        return ime;
    }

    public void setIme(String ime) {
        this.ime = ime;
    }
}
