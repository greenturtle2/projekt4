package progi.fizikalna_terapija_v1.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import progi.fizikalna_terapija_v1.dao.UredajRepository;
import progi.fizikalna_terapija_v1.domain.Uredaj;
import progi.fizikalna_terapija_v1.dto.UredajDto;
import progi.fizikalna_terapija_v1.service.UredajService;

import java.util.List;

@Service
public class UredajServiceJpa implements UredajService {

    @Autowired
    private UredajRepository uredajRepository;

    @Override
    public List<Uredaj> listAll() {
        return uredajRepository.findAll();
    }

    @Override
    public void saveUredaj(UredajDto u) {
        Uredaj uredaj = new Uredaj(u.getNazivUredaja(), u.getSlikaUredaja());
        uredajRepository.save(uredaj);
        return;
    }

    /*@Override
    public List<Uredaj> showZauzeteUredaje() {
        List<Uredaj> uredajs = uredajRepository.listTakenDevices();
        return uredajs;
    }*/


}
