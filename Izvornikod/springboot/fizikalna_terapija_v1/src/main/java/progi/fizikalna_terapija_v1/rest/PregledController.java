package progi.fizikalna_terapija_v1.rest;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import progi.fizikalna_terapija_v1.domain.Bolesnik;
import progi.fizikalna_terapija_v1.domain.TerminTest;
import progi.fizikalna_terapija_v1.dto.PregledDto;
import progi.fizikalna_terapija_v1.service.BolesnikService;
import progi.fizikalna_terapija_v1.service.TerminTestService;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/")
public class PregledController {

    @Autowired
    private BolesnikService bolesnikService;

    @Autowired
    private TerminTestService terminTestService;

    @PostMapping("/pregled")
    public List<String> ispisiPregled(@RequestBody PregledDto pregledDto){
        LocalDate datum = pregledDto.getDatum();
        String vrsta = pregledDto.getVrsta();
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");

        List<String> redoviZaIspis = new ArrayList<>();

        if(vrsta.equals("Bolesnik")){
            String mbo = pregledDto.getIme();
            Bolesnik bolesnik = bolesnikService.searchBolesnikById(mbo).get(0);
            String bolesnik_id = bolesnik.getId().toString();

            List<TerminTest> terminTests = terminTestService.poBolesniku(bolesnik_id);



            for(TerminTest terminTest : terminTests){
                String jedanRed = "";
                LocalDate dan2 = terminTest.getDan();
                java.time.DayOfWeek danUTjednu;
                int redniBrojDana;
                for(int i = 0; i<10; i++){
                    jedanRed = "";
                    danUTjednu = dan2.getDayOfWeek();
                    redniBrojDana = danUTjednu.getValue();
                    if(redniBrojDana==6 || redniBrojDana==7){
                        i--;
                    }else{
                        if(datum.equals(dan2)){
                            jedanRed = jedanRed + bolesnik.getIme();
                            jedanRed = jedanRed + " ";
                            jedanRed = jedanRed + bolesnik.getPrezime();
                            jedanRed = jedanRed + " - ";
                            jedanRed = jedanRed + terminTest.getUsluga();
                            jedanRed = jedanRed + " - ";
                            jedanRed = jedanRed + datum.format(dateTimeFormatter);
                            jedanRed = jedanRed + " u ";
                            jedanRed = jedanRed + terminTest.getVrijeme();
                            redoviZaIspis.add(jedanRed);
                        }
                    }
                    dan2 = dan2.plusDays(1);
                }
            }

        }else{
            String usluga = pregledDto.getIme();

            List<TerminTest> terminTests = terminTestService.poUsluzi(usluga);

            for(TerminTest terminTest : terminTests){
                String jedanRed = "";
                LocalDate dan2 = terminTest.getDan();
                java.time.DayOfWeek danUTjednu;
                int redniBrojDana;
                for(int i = 0; i<10; i++){
                    jedanRed = "";
                    danUTjednu = dan2.getDayOfWeek();
                    redniBrojDana = danUTjednu.getValue();
                    if(redniBrojDana==6 || redniBrojDana==7){
                        i--;
                    }else{
                        if(datum.equals(dan2)){
                            Long id = Long.parseLong(terminTest.getBolesnik_id());
                            Bolesnik bolesnik = bolesnikService.poIdu(id).get(0);
                            jedanRed = jedanRed + terminTest.getUsluga();
                            jedanRed = jedanRed + " - ";
                            jedanRed = jedanRed + datum.format(dateTimeFormatter);
                            jedanRed = jedanRed + " u ";
                            jedanRed = jedanRed + terminTest.getVrijeme();
                            jedanRed = jedanRed + " - ";
                            jedanRed = jedanRed + bolesnik.getIme();
                            jedanRed = jedanRed + " ";
                            jedanRed = jedanRed + bolesnik.getPrezime();
                            redoviZaIspis.add(jedanRed);
                        }
                    }
                    dan2 = dan2.plusDays(1);
                }
            }
        }
        return redoviZaIspis;
    }

}
