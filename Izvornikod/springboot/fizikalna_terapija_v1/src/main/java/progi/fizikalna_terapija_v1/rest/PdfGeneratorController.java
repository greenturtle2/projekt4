package progi.fizikalna_terapija_v1.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import progi.fizikalna_terapija_v1.service.impl.PdfGeneratorService;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

@CrossOrigin(origins = "*", maxAge = 3600)
@Controller
public class PdfGeneratorController {

    @Autowired
    private PdfGeneratorService pdfGeneratorService;

    @GetMapping("/pdf/generiraj/{mbo}")
    public void generirajPdf(HttpServletResponse response, @PathVariable(value = "mbo") String mbo) throws IOException {

        String tekst = "Ovo je novi tekst za paragraf!";

        response.setContentType("application/pdf");
        DateFormat dateFormatter = new SimpleDateFormat("dd-MM-yyyy");
        String currentDateTime = dateFormatter.format(new Date());

        String headerKey = "Content-Disposition";
        String headerValue = "attachment; filename=dokument_izvjesce_" + currentDateTime + ".pdf";
        response.setHeader(headerKey, headerValue);

        pdfGeneratorService.export(response, mbo);
    }


}
