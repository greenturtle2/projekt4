package progi.fizikalna_terapija_v1.dto;

public class EmailDto {

    private String primatelj;
    private String predmet;
    private String poruka;

    public EmailDto(){

    }

    public EmailDto(String primatelj, String predmet, String poruka){
        this.primatelj = primatelj;
        this.predmet = predmet;
        this.poruka = poruka;
    }

    public EmailDto(String primatelj) {
        this.primatelj = primatelj;
    }

    public String getPrimatelj() {
        return primatelj;
    }

    public void setPrimatelj(String primatelj) {
        this.primatelj = primatelj;
    }

    public String getPredmet() {
        return predmet;
    }

    public void setPredmet(String predmet) {
        this.predmet = predmet;
    }

    public String getPoruka() {
        return poruka;
    }

    public void setPoruka(String poruka) {
        this.poruka = poruka;
    }
}
