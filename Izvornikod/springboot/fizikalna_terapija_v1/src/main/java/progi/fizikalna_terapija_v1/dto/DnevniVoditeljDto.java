package progi.fizikalna_terapija_v1.dto;

import java.time.LocalDate;
import java.util.Date;

public class DnevniVoditeljDto {

    private Long id;

    private LocalDate danVodenja;

    private String doktor_id;

    public DnevniVoditeljDto() {}

    public DnevniVoditeljDto(Long id, LocalDate danVodenja, String doktor_id) {
        this.id = id;
        this.danVodenja = danVodenja;
        this.doktor_id = doktor_id;
    }

    public Long getId() {
        return this.id;
    }

    public LocalDate getDanVodenja() {
        return this.danVodenja;
    }

    public void setDanVodenja(LocalDate danVodenja) {
        this.danVodenja = danVodenja;
    }

    public String getDoktor_id() {
        return this.doktor_id;
    }

    public void setDoktor_id(String doktor_id) {
        this.doktor_id = doktor_id;
    }
}