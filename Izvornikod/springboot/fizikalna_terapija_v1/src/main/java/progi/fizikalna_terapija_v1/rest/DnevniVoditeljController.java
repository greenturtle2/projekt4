package progi.fizikalna_terapija_v1.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import progi.fizikalna_terapija_v1.domain.DnevniVoditelj;
import progi.fizikalna_terapija_v1.service.DnevniVoditeljService;

import java.time.LocalDate;
import java.util.List;

@RestController
@RequestMapping("/dnevnivoditelj")
@CrossOrigin(origins = "*", maxAge = 3600)
public class DnevniVoditeljController {

    @Autowired
    private DnevniVoditeljService dnevniVoditeljService;

    @GetMapping("")
    public List<DnevniVoditelj> listDnevniVoditelj(){
        return dnevniVoditeljService.listAll();
    }

    @GetMapping("/{danVodenja}") 
    public String searchDnevniVoditeljs(@PathVariable("danVodenja")
                                                          @DateTimeFormat(pattern = "yyyy-MM-dd")
                                                          LocalDate danVodenja) {
        return dnevniVoditeljService.getDnevniVoditeljByDate(danVodenja).get(0).getDoktor_id();
    }
    
}
