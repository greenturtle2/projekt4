package progi.fizikalna_terapija_v1.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import progi.fizikalna_terapija_v1.domain.EUloga;
import progi.fizikalna_terapija_v1.domain.Uloga;

import java.util.Optional;

@Repository
public interface UlogaRepositroy extends JpaRepository<Uloga, Long> {

    Optional<Uloga> findByName(EUloga naziv);
}
