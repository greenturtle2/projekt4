package progi.fizikalna_terapija_v1.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import progi.fizikalna_terapija_v1.dao.BolesnikUslugeRepository;
import progi.fizikalna_terapija_v1.dto.*;
import progi.fizikalna_terapija_v1.service.*;

@RestController
@CrossOrigin(origins = "*", maxAge = 3600)
@RequestMapping("/add")
public class UslugaController {

    @Autowired
    private UslugaService uslugaService;

    @Autowired
    private EvidencijaService evidencijaService;

    @Autowired
    private BolesnikUslugeService bolesnikUslugeService;

    @Autowired
    private BolesnikUslugeRepository bolesnikUslugeRepository;

    @Autowired
    private ZavrsetakService zavrsetakService;

    @Autowired
    private DnevniVoditeljService dnevniVoditeljService;

    @PostMapping("/usluga")
    public String saveUsluga(@RequestBody UslugaDto uslugaDto){
        String naziv = uslugaService.addUsluga(uslugaDto);
        return naziv;
    }

    @PostMapping("/evidencija")
    public String saveEvidencija(@RequestBody EvidencijaDto evidencijaDto){
        String id_bolesnik = evidencijaService.addEvidencija(evidencijaDto);
        return id_bolesnik;
    }

    @PostMapping("/bolesnik-usluga")
    public String saveBolesnikUsluge(@RequestBody BolesnikUslugeDto bolesnikUslugeDto){
        String usluga = bolesnikUslugeService.addBolesnikUsluge(bolesnikUslugeDto);
        return usluga;
    }

    @PostMapping("/zavrsetak")
    public String saveZavrsetak(@RequestBody ZavrsetakDto zavrsetakDto){
        String bolesnik = zavrsetakService.addZavrsetak(zavrsetakDto);
        return bolesnik;
    }

    @PostMapping("/dnevnivoditelj")
    public String saveDnevniVoditelj(@RequestBody DnevniVoditeljDto dnevniVoditeljDto){
        String dnevni = dnevniVoditeljService.addDnevniVoditelj(dnevniVoditeljDto);
        return dnevni;
    }
}
