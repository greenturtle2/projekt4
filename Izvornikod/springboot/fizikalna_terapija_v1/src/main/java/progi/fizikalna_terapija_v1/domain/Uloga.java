package progi.fizikalna_terapija_v1.domain;

import javax.persistence.*;

@Entity
@Table(name = "Uloga")
public class Uloga {

    @Id
    @GeneratedValue
    private Integer id;

    @Enumerated(EnumType.STRING)
    private EUloga name;

    public Uloga(){}

    public Uloga(EUloga naziv) {
        this.name = naziv;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public EUloga getNaziv() {
        return name;
    }

    public void setNaziv(EUloga naziv) {
        this.name = naziv;
    }
}
