package progi.fizikalna_terapija_v1.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import progi.fizikalna_terapija_v1.dao.TerminTestRepository;
import progi.fizikalna_terapija_v1.domain.TerminTest;
import progi.fizikalna_terapija_v1.dto.TerminTestDto;
import progi.fizikalna_terapija_v1.service.TerminTestService;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;

@Service
public class TerminTestServiceImpl implements TerminTestService {

    @Autowired
    private TerminTestRepository terminTestRepository;

    @Override
    public List<TerminTest> listAll(){
        return terminTestRepository.findAll();
    }

    @Override
    public void addTerminTest(TerminTestDto terminTestDto){
        TerminTest terminTest = new TerminTest(terminTestDto.getDan(), terminTestDto.getVrijeme(),
                                                terminTestDto.getBolesnik_id(), terminTestDto.getUsluga());
        terminTestRepository.save(terminTest);
    }

    @Override
    public List<TerminTest> poDanu(LocalDate dan){
        List<TerminTest> terminTests = terminTestRepository.poDanu(dan);
        return terminTests;
    }

    @Override
    public List<TerminTest> poVri(LocalTime vri){
        List<TerminTest> terminTests = terminTestRepository.poVri(vri);
        return terminTests;
    }

    @Override
    public List<TerminTest> poDanuIVri(LocalDate dan, LocalTime vri){
        List<TerminTest> terminTests = terminTestRepository.poDanuIVri(dan, vri);
        return terminTests;
    }

    @Override
    public List<TerminTest> preklapanje(LocalDate dan, LocalTime poc, LocalTime kra){
        List<TerminTest> terminTests = terminTestRepository.preklapanje(dan, poc, kra);
        return terminTests;
    }

    @Override
    public List<TerminTest> poUsluziIVri(String usluga, LocalTime poc, LocalTime kra){
        List<TerminTest> terminTests = terminTestRepository.poUsluziIVri(usluga, poc, kra);
        return terminTests;
    }

    @Override
    public List<TerminTest> poBolesniku(String bolesnik_id){
        List<TerminTest> terminTests = terminTestRepository.poBolesniku(bolesnik_id);
        return terminTests;
    }

    @Override
    public List<TerminTest> poUsluzi(String usluga){
        List<TerminTest> terminTests = terminTestRepository.poUsluzi(usluga);
        return terminTests;
    }
}
