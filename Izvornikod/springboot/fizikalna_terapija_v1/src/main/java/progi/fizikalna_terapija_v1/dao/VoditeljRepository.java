package progi.fizikalna_terapija_v1.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import progi.fizikalna_terapija_v1.domain.Voditelj;

import java.util.List;

public interface VoditeljRepository extends JpaRepository<Voditelj, Long> {

    List<Voditelj> searchVoditeljByEmail(String email);

}
