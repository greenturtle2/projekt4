package progi.fizikalna_terapija_v1.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Bolesnik {

    @Id
    @GeneratedValue
    private Long id;

    @Column(nullable = false)
    private String ime;

    @Column(nullable = false)
    private String prezime;

    @Column(nullable = false)
    private String telefon;

    private String email;

    @Column(nullable = false)
    private String dijagnoza;

    @Column(nullable = false)
    private String mbo;

    @Column(nullable = false)
    private String dodatno_zdravstveno;

    @Column(nullable = false)
    private String moj_lijecnik;



    public Long getId() {
        return id;
    }

    public String getIme() {
        return ime;
    }

    public void setIme(String ime) {
        this.ime = ime;
    }

    public String getPrezime() {
        return prezime;
    }

    public void setPrezime(String prezime) {
        this.prezime = prezime;
    }

    public String getTelefon() {
        return telefon;
    }

    public void setTelefon(String telefon) {
        this.telefon = telefon;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getDijagnoza() {
        return dijagnoza;
    }

    public void setDijagnoza(String dijagnoza) {
        this.dijagnoza = dijagnoza;
    }

    public String getMbo() {
        return mbo;
    }

    public void setMbo(String mbo) {
        this.mbo = mbo;
    }

    public String getDodatno_zdravstveno() {
        return dodatno_zdravstveno;
    }

    public void setDodatno_zdravstveno(String dodatno_zdravstveno) {
        this.dodatno_zdravstveno = dodatno_zdravstveno;
    }

    public String getMoj_lijecnik() {
        return moj_lijecnik;
    }

    public void setMoj_lijecnik(String moj_lijecnik) {
        this.moj_lijecnik = moj_lijecnik;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Bolesnik(){}



    public Bolesnik(String ime, String prezime, String telefon, String email, String dijagnoza,
                    String mbo, String dodatno_zdravstveno, String moj_lijecnik) {
        this.ime = ime;
        this.prezime = prezime;
        this.telefon = telefon;
        this.email = email;
        this.dijagnoza = dijagnoza;
        this.mbo = mbo;
        this.dodatno_zdravstveno = dodatno_zdravstveno;
        this.moj_lijecnik = moj_lijecnik;
    }

    @Override
    public String toString() {
        return "Ime: " + ime + "\n" +
                "Prezime: " + prezime + "\n" +
                "Matični broj osiguranika: " + mbo + "\n" +
                "Broj telefona: " + telefon + "\n" +
                "E-mail: " + email + "\n" +
                "Dijagnoza: " + dijagnoza + "\n" +
                "Dodatno zdravstveno osiguranje: " + dodatno_zdravstveno + "\n";
    }
}
