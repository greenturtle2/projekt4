package progi.fizikalna_terapija_v1.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import progi.fizikalna_terapija_v1.domain.Bolesnik;

import java.util.List;

public interface BolesnikRepository extends JpaRepository<Bolesnik, Long> {

    @Query("SELECT b FROM Bolesnik b WHERE " +
            "b.moj_lijecnik LIKE CONCAT('%', :query, '%')")
    List<Bolesnik> searchBolesniks(String query);



    @Query("SELECT b FROM Bolesnik b where " +
            "b.mbo LIKE CONCAT('%', :query, '%')")
    List<Bolesnik> searchBolesnikById(String query);

    @Query("SELECT b FROM Bolesnik b WHERE b.id=?1")
    List<Bolesnik> poIdu(Long id);



}
