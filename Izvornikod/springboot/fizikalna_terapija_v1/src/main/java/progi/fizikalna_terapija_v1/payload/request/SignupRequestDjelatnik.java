package progi.fizikalna_terapija_v1.payload.request;

import java.util.Set;
import javax.validation.constraints.*;

public class SignupRequestDjelatnik {

    private String ime;
    private String prezime;
    private String strucna_sprema;
    private String specijalizacija;
    private String email;
    private String telefon;

    private Set<String> uloga;

    public String getIme() {
        return ime;
    }

    public void setIme(String ime) {
        this.ime = ime;
    }

    public String getPrezime() {
        return prezime;
    }

    public void setPrezime(String prezime) {
        this.prezime = prezime;
    }

    public String getStrucna_sprema() {
        return strucna_sprema;
    }

    public void setStrucna_sprema(String strucna_sprema) {
        this.strucna_sprema = strucna_sprema;
    }

    public String getSpecijalizacija() {
        return specijalizacija;
    }

    public void setSpecijalizacija(String specijalizacija) {
        this.specijalizacija = specijalizacija;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTelefon() {
        return telefon;
    }

    public void setTelefon(String telefon) {
        this.telefon = telefon;
    }



    public Set<String> getUloga() {
        return uloga;
    }

    public void setUloga(Set<String> uloga) {
        this.uloga = uloga;
    }
}
