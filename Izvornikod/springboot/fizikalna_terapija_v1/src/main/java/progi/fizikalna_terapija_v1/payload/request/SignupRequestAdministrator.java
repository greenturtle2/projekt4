package progi.fizikalna_terapija_v1.payload.request;

import java.util.Set;

public class SignupRequestAdministrator {

    private String email;

    private String password;


    private Set<String> uloga;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Set<String> getUloga() {
        return uloga;
    }

    public void setUloga(Set<String> uloga) {
        this.uloga = uloga;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
