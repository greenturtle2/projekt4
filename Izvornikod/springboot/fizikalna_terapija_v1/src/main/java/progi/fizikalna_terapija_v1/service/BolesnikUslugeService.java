package progi.fizikalna_terapija_v1.service;

import progi.fizikalna_terapija_v1.domain.BolesnikUsluge;
import progi.fizikalna_terapija_v1.dto.BolesnikUslugeDto;

import java.util.List;

public interface BolesnikUslugeService {

    String addBolesnikUsluge(BolesnikUslugeDto bolesnikUslugeDto);

    List<BolesnikUsluge> searchBolesnikUsluge(String query);
}
