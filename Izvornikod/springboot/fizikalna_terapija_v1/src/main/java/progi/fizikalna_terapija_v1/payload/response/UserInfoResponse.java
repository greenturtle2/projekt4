package progi.fizikalna_terapija_v1.payload.response;

import java.util.List;

public class UserInfoResponse {

    private Long id;
    private String username;
    private List<String> roles;

    private String ime;




    public UserInfoResponse(Long id, String username, List<String> roles, String ime) {
        this.id = id;
        this.username = username;
        this.roles = roles;
        this.ime = ime;
    }



    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public List<String> getRoles() {
        return roles;
    }

    public String getIme() {
        return ime;
    }

    public void setIme(String ime) {
        this.ime = ime;
    }


}
