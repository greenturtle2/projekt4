package progi.fizikalna_terapija_v1.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import progi.fizikalna_terapija_v1.domain.Bolesnik;
import progi.fizikalna_terapija_v1.service.BolesnikService;

import java.util.List;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/")
public class BolesnikController {

    @Autowired
    private BolesnikService bolesnikService;

    @GetMapping("/bolesnik")
    @PreAuthorize("hasRole('BOLESNIK')")
    public String bolesnikAccess(){
        return "Stranica BOLESNIK.";
    }

    @GetMapping("/bolesnici")
    public List<Bolesnik> listBolesnici(){
        return bolesnikService.listAll();
    }

    @GetMapping("/bolesnici/moj_lijecnik/{id}")
    public ResponseEntity<List<Bolesnik>> searchBolesniks(@PathVariable(value = "id") String query){
        return ResponseEntity.ok(bolesnikService.searchBolesniks(query));
    }

    @GetMapping("bolesnik/{id}")
    public ResponseEntity<List<Bolesnik>> searchBolesnikById(@PathVariable(value = "id") String query){
        return ResponseEntity.ok(bolesnikService.searchBolesnikById(query));
    }
}
