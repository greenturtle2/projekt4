package progi.fizikalna_terapija_v1.domain;

import javax.persistence.*;

@Entity
public class Voditelj {

    @Id
    @GeneratedValue
    private Long id;

    @Column(nullable = false)
    private String email;

    @Transient
    private String password;

    public Long getId() {
        return id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Voditelj(){}

    public Voditelj(String email, String password){
        this.email = email;
        this.password = password;
    }
}
