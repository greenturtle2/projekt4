package progi.fizikalna_terapija_v1.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import progi.fizikalna_terapija_v1.domain.Korisnik;

import java.util.Optional;

@Repository
public interface KorisnikRepository extends JpaRepository<Korisnik, Long> {

    Optional<Korisnik> findByUsername(String username);

    Boolean existsByUsername(String username);

    @Query("SELECT k FROM Korisnik k WHERE k.username=?1")
    Korisnik vratiKorisnika(String username);
}
