package progi.fizikalna_terapija_v1;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import progi.fizikalna_terapija_v1.service.impl.EmailService;

@SpringBootApplication
public class FizikalnaTerapijaV1Application {

	/*##################################*/
	@Autowired
	private EmailService emailService;

	public static void main(String[] args) {
		SpringApplication.run(FizikalnaTerapijaV1Application.class, args);
	}

	/*##################################*/
	@EventListener(ApplicationReadyEvent.class)
	public void sendMail(){
		emailService.sendEmail("riwaka1346@dentaltz.com", "PASSWORD RECOVERY",
								"Vaša šifra je IDsdf_4&");
	}

}
