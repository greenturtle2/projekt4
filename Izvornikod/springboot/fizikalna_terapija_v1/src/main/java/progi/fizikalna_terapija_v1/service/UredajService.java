package progi.fizikalna_terapija_v1.service;

import org.springframework.stereotype.Service;
import progi.fizikalna_terapija_v1.domain.Uredaj;
import progi.fizikalna_terapija_v1.dto.UredajDto;

import java.util.List;


public interface UredajService {

    List<Uredaj> listAll();
    void saveUredaj(UredajDto u);

    /*List<Uredaj> showZauzeteUredaje();*/
}
