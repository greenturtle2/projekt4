package progi.fizikalna_terapija_v1.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import progi.fizikalna_terapija_v1.domain.Djelatnik;

import java.util.List;

public interface DjelatnikRepository extends JpaRepository<Djelatnik, Long> {

    List<Djelatnik> searchDjelatnikById(Long id);

    List<Djelatnik> searchDjelatnikByEmail(String email);

}
