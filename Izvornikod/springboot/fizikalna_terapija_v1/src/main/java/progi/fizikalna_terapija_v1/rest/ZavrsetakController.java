package progi.fizikalna_terapija_v1.rest;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import progi.fizikalna_terapija_v1.domain.Zavrsetak;
import progi.fizikalna_terapija_v1.service.ZavrsetakService;

import java.util.List;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/zavrsetak")
public class ZavrsetakController {

    @Autowired
    private ZavrsetakService zavrsetakService;

    @GetMapping("/{id}")
    public ResponseEntity<List<Zavrsetak>> searchZavrsetak(@PathVariable(value = "id") String query){
        return ResponseEntity.ok(zavrsetakService.searchZavrsetak(query));
    }

}
