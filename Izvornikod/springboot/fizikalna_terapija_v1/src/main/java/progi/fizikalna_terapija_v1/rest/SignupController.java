package progi.fizikalna_terapija_v1.rest;

import org.passay.CharacterRule;
import org.passay.EnglishCharacterData;
import org.passay.PasswordGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;
import progi.fizikalna_terapija_v1.dao.*;
import progi.fizikalna_terapija_v1.domain.*;
import progi.fizikalna_terapija_v1.payload.request.SignupRequestAdministrator;
import progi.fizikalna_terapija_v1.payload.request.SignupRequestBolesnik;
import progi.fizikalna_terapija_v1.payload.request.SignupRequestDjelatnik;
import progi.fizikalna_terapija_v1.payload.request.SignupRequestVoditelj;
import progi.fizikalna_terapija_v1.payload.response.MessageResponse;
import progi.fizikalna_terapija_v1.payload.response.RegistracijaBolesnikaResponse;
import progi.fizikalna_terapija_v1.security.jwt.JwtUtils;

import javax.management.relation.Role;
import javax.validation.Valid;
import java.util.HashSet;
import java.util.Set;



@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/signup")
public class SignupController {

    private String sifra;

    public String generirajSifru(){
        CharacterRule mala_slova = new CharacterRule(EnglishCharacterData.LowerCase);
        CharacterRule velika_slova = new CharacterRule(EnglishCharacterData.UpperCase);
        CharacterRule brojevi = new CharacterRule(EnglishCharacterData.Digit);
        CharacterRule znakovi = new CharacterRule(EnglishCharacterData.Special);

        mala_slova.setNumberOfCharacters(2);
        velika_slova.setNumberOfCharacters(2);
        brojevi.setNumberOfCharacters(2);
        znakovi.setNumberOfCharacters(1);

        PasswordGenerator generator = new PasswordGenerator();

        String sifra = generator.generatePassword(8, mala_slova, velika_slova, brojevi, znakovi);

        return sifra;
    }

    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    KorisnikRepository korisnikRepository;

    @Autowired
    UlogaRepositroy ulogaRepositroy;

    @Autowired
    PasswordEncoder encoder;

    @Autowired
    JwtUtils jwtUtils;

    @Autowired
    DjelatnikRepository djelatnikRepository;

    @Autowired
    AdministratorRepository administratorRepository;

    @Autowired
    VoditeljRepository voditeljRepository;

    @Autowired
    BolesnikRepository bolesnikRepository;

    @PostMapping("/d")
    public ResponseEntity<?> registerUser(@Valid @RequestBody SignupRequestDjelatnik signupRequestDjelatnik){
        if(korisnikRepository.existsByUsername(signupRequestDjelatnik.getEmail())){
            return ResponseEntity.badRequest().body(new MessageResponse("Error: username je vec zauzet!"));
        }

        sifra = generirajSifru();

        Korisnik korisnik = new Korisnik(signupRequestDjelatnik.getEmail(),
                encoder.encode(sifra), signupRequestDjelatnik.getIme());

        Set<String> strUloge = signupRequestDjelatnik.getUloga();
        Set<Uloga> uloge = new HashSet<>();

        Uloga userRole = ulogaRepositroy.findByName(EUloga.ROLE_MEDIC)
                .orElseThrow(() -> new RuntimeException("Error: Uloga ne postoji."));
        uloge.add(userRole);

        korisnik.setUloge(uloge);
        korisnikRepository.save(korisnik);

        Djelatnik djelatnik = new Djelatnik(signupRequestDjelatnik.getIme(), signupRequestDjelatnik.getPrezime(),
                                    signupRequestDjelatnik.getStrucna_sprema(),signupRequestDjelatnik.getSpecijalizacija(),
                                    signupRequestDjelatnik.getEmail(), signupRequestDjelatnik.getTelefon());

        djelatnikRepository.save(djelatnik);


        return ResponseEntity.ok(new MessageResponse("Korisnik uspjesno registriran! Generirana sifra je " + sifra));
    }

//#############################################################################################################

    @PostMapping("/a")
    public ResponseEntity<?> registerUser(@Valid @RequestBody SignupRequestAdministrator signupRequestAdministrator){
        if(korisnikRepository.existsByUsername(signupRequestAdministrator.getEmail())){
            return ResponseEntity.badRequest().body(new MessageResponse("Error: username je vec zauzet!"));
        }

        //sifra = generirajSifru();

        Korisnik korisnik = new Korisnik(signupRequestAdministrator.getEmail(),
                encoder.encode(signupRequestAdministrator.getPassword()), "Admin");

        Set<String> strUloge = signupRequestAdministrator.getUloga();
        Set<Uloga> uloge = new HashSet<>();

        Uloga userRole = ulogaRepositroy.findByName(EUloga.ROLE_ADMIN)
                .orElseThrow(() -> new RuntimeException("Error: Uloga ne postoji."));
        uloge.add(userRole);

        korisnik.setUloge(uloge);
        korisnikRepository.save(korisnik);

        Administrator administrator = new Administrator(signupRequestAdministrator.getEmail(),
                                                        signupRequestAdministrator.getPassword());

        administratorRepository.save(administrator);


        return ResponseEntity.ok(new MessageResponse("Korisnik uspjesno registriran!"));
    }

//##############################################################################################################

    @PostMapping("/v")
    public ResponseEntity<?> registerUser(@Valid @RequestBody SignupRequestVoditelj signupRequestVoditelj){
        if(korisnikRepository.existsByUsername(signupRequestVoditelj.getEmail())){
            return ResponseEntity.badRequest().body(new MessageResponse("Error: username je vec zauzet!"));
        }

        //sifra = generirajSifru();

        Korisnik korisnik = new Korisnik(signupRequestVoditelj.getEmail(),
                encoder.encode(signupRequestVoditelj.getPassword()), "Voditelj");

        Set<String> strUloge = signupRequestVoditelj.getUloga();
        Set<Uloga> uloge = new HashSet<>();

        Uloga userRole = ulogaRepositroy.findByName(EUloga.ROLE_VODITELJ)
                .orElseThrow(() -> new RuntimeException("Error: Uloga ne postoji."));
        uloge.add(userRole);

        korisnik.setUloge(uloge);
        korisnikRepository.save(korisnik);

        Voditelj voditelj = new Voditelj(signupRequestVoditelj.getEmail(),
                signupRequestVoditelj.getPassword());

        voditeljRepository.save(voditelj);


        return ResponseEntity.ok(new MessageResponse("Korisnik uspjesno registriran!"));
    }

//##############################################################################################################

    @PostMapping("/b")
    public ResponseEntity<?> registerUser(@Valid @RequestBody SignupRequestBolesnik signupRequestBolesnik){
        if(korisnikRepository.existsByUsername(signupRequestBolesnik.getMbo())){
            return ResponseEntity.badRequest().body(new MessageResponse("Error: bolesnik je vec u sustavu!"));
        }

        sifra = generirajSifru();

        Korisnik korisnik = new Korisnik(signupRequestBolesnik.getMbo(),
                encoder.encode(sifra), signupRequestBolesnik.getIme());

        Set<String> strUloge = signupRequestBolesnik.getUloga();
        Set<Uloga> uloge = new HashSet<>();

        Uloga userRole = ulogaRepositroy.findByName(EUloga.ROLE_BOLESNIK)
                .orElseThrow(() -> new RuntimeException("Error: Uloga ne postoji."));
        uloge.add(userRole);

        korisnik.setUloge(uloge);
        korisnikRepository.save(korisnik);

        Bolesnik bolesnik = new Bolesnik(signupRequestBolesnik.getIme(), signupRequestBolesnik.getPrezime(),
                signupRequestBolesnik.getTelefon(), signupRequestBolesnik.getEmail(), signupRequestBolesnik.getDijagnoza(),
                signupRequestBolesnik.getMbo(), signupRequestBolesnik.getDodatno_zdravstveno(),
                signupRequestBolesnik.getMoj_lijecnik());

        bolesnikRepository.save(bolesnik);

        String id = String.valueOf(bolesnik.getId());

        String poruka = "Bolesnik uspjesno registriran! Generirana sifra je " + sifra;

        /*
        return ResponseEntity.ok(new MessageResponse("Bolesnik uspjesno registriran! Generirana sifra je " + sifra +
                                                        " , a id je " + id)); */

        return ResponseEntity.ok(new RegistracijaBolesnikaResponse(poruka,id));
    }
}
