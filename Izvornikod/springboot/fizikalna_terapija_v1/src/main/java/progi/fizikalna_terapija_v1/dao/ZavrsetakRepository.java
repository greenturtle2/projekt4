package progi.fizikalna_terapija_v1.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import progi.fizikalna_terapija_v1.domain.Zavrsetak;

import java.util.List;

@Repository
public interface ZavrsetakRepository extends JpaRepository<Zavrsetak, Long> {

    @Query("SELECT z FROM Zavrsetak z WHERE " +
            "z.bolesnik LIKE CONCAT('%', :query, '%')")
    List<Zavrsetak> searchZavrsetak(String query);

}
