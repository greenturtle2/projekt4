package progi.fizikalna_terapija_v1.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import progi.fizikalna_terapija_v1.dao.UslugaRepository;
import progi.fizikalna_terapija_v1.domain.Usluga;
import progi.fizikalna_terapija_v1.dto.UslugaDto;
import progi.fizikalna_terapija_v1.service.UslugaService;

import java.util.List;

@Service
public class UslugaServiceImpl implements UslugaService {

    @Autowired
    private UslugaRepository uslugaRepository;

    @Override
    public String addUsluga(UslugaDto uslugaDto) {
        Usluga usluga = new Usluga(uslugaDto.getNaziv(), uslugaDto.getOpis(), uslugaDto.getSlika(),
                                    uslugaDto.getVrijeme(), uslugaDto.getParalelno());
        uslugaRepository.save(usluga);
        return usluga.getNaziv();

    }

    public List<Usluga> listAll(){
        return uslugaRepository.findAll();
    }

    @Override
    public List<Usluga> vratiUslugu(String naziv){
        List<Usluga> usluge = uslugaRepository.vratiUslugu(naziv);
        return usluge;
    }


}
