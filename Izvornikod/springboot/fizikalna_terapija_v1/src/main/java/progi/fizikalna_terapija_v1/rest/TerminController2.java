package progi.fizikalna_terapija_v1.rest;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import progi.fizikalna_terapija_v1.dto.TerminDto;
import progi.fizikalna_terapija_v1.dto.TerminTestDto;
import progi.fizikalna_terapija_v1.service.TerminService;
import progi.fizikalna_terapija_v1.service.TerminTestService;

import java.util.List;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/add")
public class TerminController2 {

    @Autowired
    private TerminService terminService;

    @Autowired
    private TerminTestService terminTestService;

    @PostMapping("/termin")
    public void saveTermin(@RequestBody TerminDto terminDto) {
        terminService.addTermin(terminDto);
        return;
    }

    @PostMapping("/termintest")
    public void saveTerminTest(@RequestBody TerminTestDto terminTestDto){
        terminTestService.addTerminTest(terminTestDto);
    }


}
