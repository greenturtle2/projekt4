package progi.fizikalna_terapija_v1.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import progi.fizikalna_terapija_v1.domain.BolesnikUsluge;

import java.util.List;

@Repository
public interface BolesnikUslugeRepository extends JpaRepository<BolesnikUsluge, Long> {

    @Query("SELECT b FROM BolesnikUsluge b WHERE " +
            "b.bolesnik_id LIKE  CONCAT('%', :query, '%')")
    List<BolesnikUsluge> searchBolesnikUsluge(String query);



}
