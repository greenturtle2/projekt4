package progi.fizikalna_terapija_v1.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import progi.fizikalna_terapija_v1.domain.TerminTest;
import progi.fizikalna_terapija_v1.domain.Usluga;
import progi.fizikalna_terapija_v1.dto.TerminTestDto;
import progi.fizikalna_terapija_v1.service.TerminTestService;
import progi.fizikalna_terapija_v1.service.UslugaService;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Calendar;
import java.util.List;

@RestController
@CrossOrigin(origins = "*", maxAge = 3600)
@RequestMapping("/provjeri")
public class ProvjeraTerminaController {

    @Autowired
    private TerminTestService terminTestService;

    @Autowired
    private UslugaService uslugaService;

    @PostMapping("/termin")
    public String termini(@RequestBody TerminTestDto terminTestDto){
        LocalDate dan = terminTestDto.getDan();
        LocalTime vri = terminTestDto.getVrijeme();
        String naziv_usluge = terminTestDto.getUsluga();


        Usluga usluga = uslugaService.vratiUslugu(naziv_usluge).get(0);
        int trajanjeMin = usluga.getVrijeme();
        int resursi = usluga.getParalelno();


        LocalTime poc = vri.plusMinutes(-trajanjeMin);
        LocalTime kra = vri.plusMinutes(trajanjeMin);

        List<TerminTest> sviTermini = terminTestService.poUsluziIVri(naziv_usluge, poc, kra);


        String preklapanje = "ne";


        int paralelnihTerpija;

        LocalDate dan2 = dan;
        java.time.DayOfWeek danUTjednu;
        int redniBrojDana;
        int istiDani;
        for(int i = 0; i<10; i++){
            istiDani = 0;
            danUTjednu = dan2.getDayOfWeek();
            redniBrojDana = danUTjednu.getValue();
            if(redniBrojDana==6 || redniBrojDana==7){
                i--;
            }else{
                for(TerminTest terminTest : sviTermini){
                    LocalDate dan_temp = terminTest.getDan();
                    java.time.DayOfWeek danUTjednuTemp;
                    int redniBrojDanaTemp;
                    for(int j = 0; j<10; j++){
                        danUTjednuTemp = dan_temp.getDayOfWeek();
                        redniBrojDanaTemp = danUTjednuTemp.getValue();
                        if(redniBrojDanaTemp==6 || redniBrojDanaTemp==7){
                            j--;
                        }else{
                            if(dan2.isEqual(dan_temp)){
                                istiDani++;
                            }
                        }
                        dan_temp = dan_temp.plusDays(1);
                    }
                }
                if(istiDani>=resursi){
                    preklapanje = "da";
                    break;
                }

            }

            dan2 = dan2.plusDays(1);
        }





        return preklapanje;
    }
}
