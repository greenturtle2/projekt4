package progi.fizikalna_terapija_v1.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import progi.fizikalna_terapija_v1.domain.BolesnikUsluge;
import progi.fizikalna_terapija_v1.service.BolesnikUslugeService;

import java.util.List;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/bolesnik-usluge")
public class BolesnikUslugeController {

    @Autowired
    private BolesnikUslugeService bolesnikUslugeService;

    @GetMapping("/{id}")
    public ResponseEntity<List<BolesnikUsluge>> searchBolesnikUsluge(@PathVariable(value = "id") String query){
        return ResponseEntity.ok(bolesnikUslugeService.searchBolesnikUsluge(query));
    }

}
