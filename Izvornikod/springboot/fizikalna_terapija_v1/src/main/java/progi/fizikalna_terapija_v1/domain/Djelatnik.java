package progi.fizikalna_terapija_v1.domain;

import javax.persistence.*;

@Entity

public class Djelatnik {



    @Id
    @GeneratedValue
    private Long id;


    @Column(nullable = false)
    private String ime;

    @Column(nullable = false)
    private String prezime;

    @Column(nullable = false)
    private String strucna_sprema;

    @Column(nullable = false)
    private String specijalizacija;

    @Column(nullable = false)
    private String email;

    @Column(nullable = false)
    private String telefon;




    public Long getId(){
        return id;
    }

    public String getIme(){
        return ime;
    }
    public void setIme(String ime){
        this.ime = ime;
    }

    public String getPrezime(){
        return prezime;
    }
    public void setPrezime(String prezime){
        this.prezime = prezime;
    }

    public String getStrucna_sprema(){
        return strucna_sprema;
    }
    public void setStrucna_sprema(String strucna_sprema){
        this.strucna_sprema = strucna_sprema;
    }

    public String getSpecijalizacija(){
        return specijalizacija;
    }
    public void setSpecijalizacija(String specijalizacija){
        this.specijalizacija = specijalizacija;
    }

    public String getEmail(){
        return email;
    }
    public void setEmail(String email){
        this.email = email;
    }

    public String getTelefon(){
        return telefon;
    }
    public void setTelefon(String telefon){
        this.telefon = telefon;
    }


    public Djelatnik(){}
    public Djelatnik(String ime, String prezime, String strucna_sprema, String specijalizacija, String email, String telefon) {
        this.ime = ime;
        this.prezime = prezime;
        this.strucna_sprema = strucna_sprema;
        this.specijalizacija = specijalizacija;
        this.email = email;
        this.telefon = telefon;
    }
}
