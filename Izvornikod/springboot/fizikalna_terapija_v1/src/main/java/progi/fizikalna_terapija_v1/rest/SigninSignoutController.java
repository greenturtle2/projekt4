package progi.fizikalna_terapija_v1.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseCookie;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;
import progi.fizikalna_terapija_v1.dao.DjelatnikRepository;
import progi.fizikalna_terapija_v1.dao.KorisnikRepository;
import progi.fizikalna_terapija_v1.dao.UlogaRepositroy;
import progi.fizikalna_terapija_v1.domain.Administrator;
import progi.fizikalna_terapija_v1.domain.Bolesnik;
import progi.fizikalna_terapija_v1.domain.Djelatnik;
import progi.fizikalna_terapija_v1.domain.Voditelj;
import progi.fizikalna_terapija_v1.payload.request.LoginRequest;
import progi.fizikalna_terapija_v1.payload.response.MessageResponse;
import progi.fizikalna_terapija_v1.payload.response.UserInfoResponse;
import progi.fizikalna_terapija_v1.security.jwt.JwtUtils;
import progi.fizikalna_terapija_v1.service.AdministratorService;
import progi.fizikalna_terapija_v1.service.BolesnikService;
import progi.fizikalna_terapija_v1.service.DjelatnikService;
import progi.fizikalna_terapija_v1.service.VoditeljService;
import progi.fizikalna_terapija_v1.service.impl.KorisnikDetailsImpl;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/")
public class SigninSignoutController {

    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    KorisnikRepository korisnikRepository;

    @Autowired
    UlogaRepositroy ulogaRepositroy;

    @Autowired
    PasswordEncoder encoder;

    @Autowired
    JwtUtils jwtUtils;

    @Autowired
    DjelatnikRepository djelatnikRepository;

    @Autowired
    private BolesnikService bolesnikService;

    @Autowired
    private DjelatnikService djelatnikService;

    @Autowired
    private AdministratorService administratorService;

    @Autowired
    private VoditeljService voditeljService;


    @PostMapping("/signin")
    public ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginRequest loginRequest) {

        Authentication authentication = authenticationManager
                .authenticate(new UsernamePasswordAuthenticationToken(loginRequest.getUsername(), loginRequest.getPassword()));

        SecurityContextHolder.getContext().setAuthentication(authentication);

        KorisnikDetailsImpl userDetails = (KorisnikDetailsImpl) authentication.getPrincipal();

        ResponseCookie jwtCookie = jwtUtils.generateJwtCookie(userDetails);

        List<String> roles = userDetails.getAuthorities().stream()
                .map(item -> item.getAuthority())
                .collect(Collectors.toList());

        String bolesnikId = "00";
        String username = userDetails.getUsername();
        List<Bolesnik> bolesnikLista = bolesnikService.searchBolesnikById(username);
        List<Djelatnik> djelatnikyLista = djelatnikService.searchDjelatnikByEmail(username);
        List<Administrator> administratorLista = administratorService.searchAdministratorByEmail(username);
        List<Voditelj> voditeljLista = voditeljService.searchVoditeljByEmail(username);
        if(bolesnikLista.size()>0){
            bolesnikId = String.valueOf(bolesnikLista.get(0).getId());
        }else if(djelatnikyLista.size()>0){
            bolesnikId = String.valueOf(djelatnikyLista.get(0).getId());
        }else if(administratorLista.size()>0){
            bolesnikId = String.valueOf(administratorLista.get(0).getId());
        }else if(voditeljLista.size()>0){
            bolesnikId = String.valueOf(voditeljLista.get(0).getId());
        }


        return ResponseEntity.ok().header(HttpHeaders.SET_COOKIE, jwtCookie.toString())
                .body(new UserInfoResponse(Long.parseLong(bolesnikId),
                        userDetails.getUsername(),
                        roles, userDetails.getIme()));
    }

    @PostMapping("/signout")
    public ResponseEntity<?> logoutUser() {
        ResponseCookie cookie = jwtUtils.getCleanJwtCookie();
        return ResponseEntity.ok().header(HttpHeaders.SET_COOKIE, cookie.toString())
                .body(new MessageResponse("Korisnik odlogiran!"));
    }

}