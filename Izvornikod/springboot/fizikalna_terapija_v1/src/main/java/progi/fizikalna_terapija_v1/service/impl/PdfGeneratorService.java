package progi.fizikalna_terapija_v1.service.impl;


import com.lowagie.text.*;
import com.lowagie.text.pdf.PdfWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import progi.fizikalna_terapija_v1.domain.*;
import progi.fizikalna_terapija_v1.service.*;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Service
public class PdfGeneratorService {

    @Autowired
    private ZavrsetakService zavrsetakService;

    @Autowired
    private BolesnikService bolesnikService;

    @Autowired
    private DjelatnikService djelatnikService;

    @Autowired
    private BolesnikUslugeService bolesnikUslugeService;

    @Autowired
    private EvidencijaService evidencijaService;



    public void export(HttpServletResponse response, String id) throws IOException {
        /*
        List<Zavrsetak> lista = zavrsetakService.searchZavrsetak(id);
        String bolesnik = lista.get(0).getBolesnik(); */

        List<Bolesnik> bolesnici = bolesnikService.searchBolesnikById(id);
        Bolesnik bolesnik = bolesnici.get(0);
        String informacije = bolesnik.toString();
        String id_bolesnika = String.valueOf(bolesnik.getId());
        Long id_doktora = Long.parseLong(bolesnik.getMoj_lijecnik());

        List<Djelatnik> djelatnici = djelatnikService.searchDjelatnikById(id_doktora);
        Djelatnik moj_doktor = djelatnici.get(0);
        String moj_doktor_ime = moj_doktor.getIme() + " " + moj_doktor.getPrezime();



        Document document = new Document(PageSize.A4);
        PdfWriter.getInstance(document, response.getOutputStream());

        document.open();
        Font fontTitle = FontFactory.getFont(FontFactory.HELVETICA_BOLD);
        fontTitle.setSize(18);

        Paragraph paragraph1 = new Paragraph("Kraj terapije - izlazni dokument", fontTitle);
        paragraph1.setAlignment(Paragraph.ALIGN_CENTER);


        Paragraph paragraph2 = new Paragraph("\n");
        paragraph2.setAlignment(Paragraph.ALIGN_LEFT);

        Paragraph paragraph3 = new Paragraph(informacije);
        paragraph3.setAlignment(Paragraph.ALIGN_LEFT);

        Paragraph paragraph4 = new Paragraph("Moj liječnik: " + moj_doktor_ime);
        paragraph4.setAlignment(Paragraph.ALIGN_LEFT);



        document.add(paragraph1);
        document.add(paragraph2);
        document.add(paragraph2);
        document.add(paragraph3);
        document.add(paragraph2);
        document.add(paragraph4);
        document.add(paragraph2);


        Paragraph paragraph5 = new Paragraph("Moje usluge:\n");
        paragraph5.setAlignment(Paragraph.ALIGN_LEFT);
        document.add(paragraph5);
        List<BolesnikUsluge> bolesnikUslugeLista = bolesnikUslugeService.searchBolesnikUsluge(id_bolesnika);
        for (BolesnikUsluge bolesnikUsluge : bolesnikUslugeLista){
            Paragraph paragraph6 = new Paragraph("--------------" + bolesnikUsluge.getUsluga() + "\n");
            paragraph6.setAlignment(Paragraph.ALIGN_LEFT);
            document.add(paragraph6);
        }

        Paragraph paragraph7 = new Paragraph("\nDani kojima sam dolazio na terapiju:                         " +
                                            "                         Liječnik koji me evidentirao:\n");
        paragraph7.setAlignment(Paragraph.ALIGN_LEFT);
        document.add(paragraph7);
        List<Evidencija> evidencijeLista = evidencijaService.searchEvidencijas(id_bolesnika);
        DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        String dan;
        for(Evidencija evidencija : evidencijeLista){
            Date datum = evidencija.getDan();
            dan = dateFormat.format(datum);
            Long doktor = Long.parseLong(evidencija.getDoktor_id());

            List<Djelatnik> djelatnici2 = djelatnikService.searchDjelatnikById(doktor);
            Djelatnik doktor2 = djelatnici2.get(0);
            String doktor2_ime = doktor2.getIme() + " " + doktor2.getPrezime();

            Paragraph paragraph8 = new Paragraph("                    " + dan + "                    " +
                    "               ----------                              " +
                                                    doktor2_ime + "\n");
            paragraph8.setAlignment(Paragraph.ALIGN_LEFT);
            document.add(paragraph8);
        }

        Paragraph paragraph9 = new Paragraph("\nZavršni razgovor:\n");
        paragraph9.setAlignment(Paragraph.ALIGN_LEFT);
        document.add(paragraph9);
        List<Zavrsetak> zavrsetakLista = zavrsetakService.searchZavrsetak(id_bolesnika);
        String zavrsni_razgovor = zavrsetakLista.get(0).toString();
        Paragraph paragraph10 = new Paragraph(zavrsni_razgovor);
        paragraph10.setAlignment(Paragraph.ALIGN_LEFT);
        document.add(paragraph10);

        document.close();

    }

}
