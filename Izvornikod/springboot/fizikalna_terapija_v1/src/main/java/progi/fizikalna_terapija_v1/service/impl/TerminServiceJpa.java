package progi.fizikalna_terapija_v1.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import progi.fizikalna_terapija_v1.dao.TerminRepository;
import progi.fizikalna_terapija_v1.domain.Termin;
import progi.fizikalna_terapija_v1.dto.TerminDto;
import progi.fizikalna_terapija_v1.exception.ApiRequestException;
import progi.fizikalna_terapija_v1.service.TerminService;

import java.util.List;

@Service
public class TerminServiceJpa implements TerminService {

    @Autowired
    private TerminRepository terminRepository;

    @Override
    public List<Termin> listAll() {
        return terminRepository.findAll();
    }

    @Override
    public void addTermin(TerminDto terminDto){
        Termin noviTermin = new Termin(terminDto.getPocetakTermina(), terminDto.getBolesnikId(), terminDto.getNaziv_usluga(), terminDto.getUsluga_trajanje());
        List<Termin> termini = terminRepository.findAll();
        for(Termin termin: termini) {
            if(noviTermin.getPocetakTermina().isAfter(termin.getPocetakTermina()) &&
            noviTermin.getPocetakTermina().isBefore(termin.getKrajTermina())&& noviTermin.getBolesnikId().equals(termin.getBolesnikId())) {
                throw new ApiRequestException("Preklapanje, isti bolesnik ne moze biti istovremeno u dva različita termina!");
            }
        }
        terminRepository.save(noviTermin);
        return;
    }

    @Override
    public void removeTermin(TerminDto terminDto) {
        Termin noviTermin = new Termin(terminDto.getPocetakTermina(), terminDto.getBolesnikId(), terminDto.getNaziv_usluga(), terminDto.getUsluga_trajanje());
        terminRepository.delete(noviTermin);
        return;
    }

    @Override
    public List<Termin> searchTermins(String query){
        List<Termin> termini = terminRepository.searchTerminsBy(query);
        return termini;
    }

}
