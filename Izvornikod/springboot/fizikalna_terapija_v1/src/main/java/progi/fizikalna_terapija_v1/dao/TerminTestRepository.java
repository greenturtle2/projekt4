package progi.fizikalna_terapija_v1.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import progi.fizikalna_terapija_v1.domain.TerminTest;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;

public interface TerminTestRepository extends JpaRepository<TerminTest, Long> {

    @Query("SELECT t FROM TerminTest t WHERE t.dan =?1")
    List<TerminTest> poDanu(LocalDate dan);

    @Query("SELECT t FROM TerminTest t WHERE t.vrijeme =?1")
    List<TerminTest> poVri(LocalTime vri);

    @Query("SELECT t FROM TerminTest t WHERE t.dan =?1 AND t.vrijeme =?2")
    List<TerminTest> poDanuIVri(LocalDate dan, LocalTime vri);

    @Query("SELECT t FROM TerminTest t WHERE t.dan =?1 AND t.vrijeme>?2 AND t.vrijeme<?3")
    List<TerminTest> preklapanje(LocalDate dan, LocalTime poc, LocalTime kra);

    @Query("SELECT t FROM TerminTest t WHERE t.usluga=?1 AND t.vrijeme>?2 AND t.vrijeme<?3")
    List<TerminTest> poUsluziIVri(String usluga, LocalTime poc, LocalTime kra);

    @Query("SELECT t FROM TerminTest t WHERE t.bolesnik_id=?1")
    List<TerminTest> poBolesniku(String bolesnik_id);

    @Query("SELECT t FROM TerminTest t WHERE t.usluga=?1")
    List<TerminTest> poUsluzi(String usluga);
}
