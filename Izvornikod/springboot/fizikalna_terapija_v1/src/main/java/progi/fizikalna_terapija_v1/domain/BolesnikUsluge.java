package progi.fizikalna_terapija_v1.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class BolesnikUsluge {

    @Id
    @GeneratedValue
    private Long id;

    private String bolesnik_id;

    private String usluga;

    public BolesnikUsluge() {
    }

    public BolesnikUsluge(String bolesnik_id, String usluga) {
        this.bolesnik_id = bolesnik_id;
        this.usluga = usluga;
    }

    public String getBolesnik_id() {
        return bolesnik_id;
    }

    public void setBolesnik_id(String bolesnik_id) {
        this.bolesnik_id = bolesnik_id;
    }

    public String getUsluga() {
        return usluga;
    }

    public void setUsluga(String usluga) {
        this.usluga = usluga;
    }
}
