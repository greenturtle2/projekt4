package progi.fizikalna_terapija_v1.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import progi.fizikalna_terapija_v1.domain.Administrator;

import java.util.List;

public interface AdministratorRepository extends JpaRepository<Administrator, Long > {

    List<Administrator> searchAdministratorByEmail(String email);
}
