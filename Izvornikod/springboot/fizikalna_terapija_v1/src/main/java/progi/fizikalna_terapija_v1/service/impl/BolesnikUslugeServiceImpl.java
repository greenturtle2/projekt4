package progi.fizikalna_terapija_v1.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import progi.fizikalna_terapija_v1.dao.BolesnikUslugeRepository;
import progi.fizikalna_terapija_v1.domain.BolesnikUsluge;
import progi.fizikalna_terapija_v1.dto.BolesnikUslugeDto;
import progi.fizikalna_terapija_v1.service.BolesnikUslugeService;

import java.util.List;

@Service
public class BolesnikUslugeServiceImpl implements BolesnikUslugeService {

    @Autowired
    private BolesnikUslugeRepository bolesnikUslugeRepository;

    public String addBolesnikUsluge(BolesnikUslugeDto bolesnikUslugeDto){
        BolesnikUsluge bolesnikUsluge = new BolesnikUsluge(bolesnikUslugeDto.getBolesnik_id(),
                                                            bolesnikUslugeDto.getUsluga());
        bolesnikUslugeRepository.save(bolesnikUsluge);
        return bolesnikUsluge.getUsluga();
    }

    @Override
    public List<BolesnikUsluge> searchBolesnikUsluge(String query){
        List<BolesnikUsluge> bolesnikUsluge = bolesnikUslugeRepository.searchBolesnikUsluge(query);
        return bolesnikUsluge;
    }
}
