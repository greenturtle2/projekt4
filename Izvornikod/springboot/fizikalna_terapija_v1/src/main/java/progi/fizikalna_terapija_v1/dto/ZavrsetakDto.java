package progi.fizikalna_terapija_v1.dto;

public class ZavrsetakDto {

    private Long id;

    private String bolesnik;

    private String napredak;

    private String zadovoljan;

    private String izjava;

    private String zapazanje;

    public ZavrsetakDto() {
    }

    public ZavrsetakDto(Long id, String bolesnik, String napredak, String zadovoljan, String izjava, String zapazanje) {
        this.id = id;
        this.bolesnik = bolesnik;
        this.napredak = napredak;
        this.zadovoljan = zadovoljan;
        this.izjava = izjava;
        this.zapazanje = zapazanje;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getBolesnik() {
        return bolesnik;
    }

    public void setBolesnik(String bolesnik) {
        this.bolesnik = bolesnik;
    }

    public String getNapredak() {
        return napredak;
    }

    public void setNapredak(String napredak) {
        this.napredak = napredak;
    }

    public String getZadovoljan() {
        return zadovoljan;
    }

    public void setZadovoljan(String zadovoljan) {
        this.zadovoljan = zadovoljan;
    }

    public String getIzjava() {
        return izjava;
    }

    public void setIzjava(String izjava) {
        this.izjava = izjava;
    }

    public String getZapazanje() {
        return zapazanje;
    }

    public void setZapazanje(String zapazanje) {
        this.zapazanje = zapazanje;
    }
}
