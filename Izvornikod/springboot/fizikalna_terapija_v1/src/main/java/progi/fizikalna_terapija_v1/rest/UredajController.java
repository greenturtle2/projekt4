package progi.fizikalna_terapija_v1.rest;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import progi.fizikalna_terapija_v1.domain.Uredaj;
import progi.fizikalna_terapija_v1.dto.UredajDto;
import progi.fizikalna_terapija_v1.service.UredajService;

import java.util.List;

@RestController
@RequestMapping("")
public class UredajController {

    @Autowired
    private UredajService uredajService;

    @GetMapping("/uredaji")

    public List<Uredaj> giveUredaji() {
        return uredajService.listAll();
    }

    @PostMapping("/add/uredaj")
    public void addUredaj(@RequestBody UredajDto uredajDto) {
        uredajService.saveUredaj(uredajDto);
        return;
    }

    /*@GetMapping("/zauzeti_uredaji")
    @PreAuthorize("hasRole('VODITELJ')")
    public List<Uredaj> zauzetiUredaji() {
        return uredajService.showZauzeteUredaje();
    }*/
}
