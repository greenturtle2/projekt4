package progi.fizikalna_terapija_v1.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import net.bytebuddy.asm.Advice;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.time.LocalDateTime;

@Entity
public class Termin {

    @Id
    @GeneratedValue
    private Long terminId;

    @Column(name = "pocetak_termina")
    @JsonFormat(pattern = "yyyy-MM-dd-HH-mm-ss")
    private LocalDateTime pocetakTermina;

    @Column(name = "kraj_termina")
    @JsonFormat(pattern = "yyyy-MM-dd-HH-mm-ss")
    private LocalDateTime krajTermina;

   @Column(name = "bolesnik_id")
   private String bolesnikId;

   @Column(name = "naziv_usluga")
   private String naziv_usluga;

   @Column(name = "trajanje_usluge") //u minutama
   private Integer usluga_trajanje;


    public Termin() {}

    public Termin(LocalDateTime pocetakTermina, LocalDateTime krajTermina, String bolesnikId, String naziv_usluga, Integer usluga_trajanje) {
        this.pocetakTermina = pocetakTermina;
        this.krajTermina = krajTermina;
        this.bolesnikId = bolesnikId;
        this.naziv_usluga = naziv_usluga;
        this.usluga_trajanje = usluga_trajanje;
    }

    public Termin(LocalDateTime pocetakTermina, String bolesnikId, String naziv_usluga, Integer usluga_trajanje) {
        this.pocetakTermina = pocetakTermina;
        this.bolesnikId = bolesnikId;
        this.naziv_usluga = naziv_usluga;
        this.krajTermina = pocetakTermina.plusDays(10);
        this.usluga_trajanje = usluga_trajanje;
    }

    public Long getTerminId() {
        return terminId;
    }

    @JsonFormat(pattern = "yyyy-MM-dd-HH-mm-ss")
    public void setTerminId(Long terminId) {
        this.terminId = terminId;
    }

    @JsonFormat(pattern = "yyyy-MM-dd-HH-mm-ss")
    public LocalDateTime getPocetakTermina() {
        return pocetakTermina;
    }

    @JsonFormat(pattern = "yyyy-MM-dd-HH-mm-ss")
    public void setPocetakTermina(LocalDateTime pocetakTermina) {
        this.pocetakTermina = pocetakTermina;
    }

    @JsonFormat(pattern = "yyyy-MM-dd-HH-mm-ss")
    public LocalDateTime getKrajTermina() {
        return krajTermina;
    }

    @JsonFormat(pattern = "yyyy-MM-dd-HH-mm-ss")
    public void setKrajTermina(LocalDateTime krajTermina) {
        this.krajTermina = krajTermina;
    }

    public String getBolesnikId() {
        return bolesnikId;
    }

    public void setBolesnikId(String bolesnikId) {
        this.bolesnikId = bolesnikId;
    }

    public String getNaziv_usluga() {
        return naziv_usluga;
    }

    public void setUslugaId(String naziv_usluga) {
        this.naziv_usluga = naziv_usluga;
    }

    public Integer getUsluga_trajanje() {
        return usluga_trajanje;
    }

    public void setUsluga_trajanje(Integer usluga_trajanje) {
        this.usluga_trajanje = usluga_trajanje;
    }
}
