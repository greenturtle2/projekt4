package progi.fizikalna_terapija_v1.dto;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.time.LocalDate;

public class PregledDto {

    @JsonFormat(pattern = "dd-MM-yyyy")
    private LocalDate datum;

    private String vrsta;

    private String ime;

    public PregledDto() {
    }

    public PregledDto(LocalDate datum, String vrsta, String ime) {
        this.datum = datum;
        this.vrsta = vrsta;
        this.ime = ime;
    }

    @JsonFormat(pattern = "dd-MM-yyyy")
    public LocalDate getDatum() {
        return datum;
    }

    @JsonFormat(pattern = "dd-MM-yyyy")
    public void setDatum(LocalDate datum) {
        this.datum = datum;
    }

    public String getVrsta() {
        return vrsta;
    }

    public void setVrsta(String vrsta) {
        this.vrsta = vrsta;
    }

    public String getIme() {
        return ime;
    }

    public void setIme(String ime) {
        this.ime = ime;
    }
}
