package progi.fizikalna_terapija_v1.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;
import progi.fizikalna_terapija_v1.dao.AdministratorRepository;
import progi.fizikalna_terapija_v1.dao.BolesnikRepository;
import progi.fizikalna_terapija_v1.domain.Administrator;
import progi.fizikalna_terapija_v1.domain.Bolesnik;
import progi.fizikalna_terapija_v1.service.BolesnikService;

import java.util.List;

@Service
public class BolesnikServiceJpa implements BolesnikService {


    @Autowired
    private BolesnikRepository bolesnikRepo;

    @Override
    public List<Bolesnik> listAll(){
        return bolesnikRepo.findAll();
    }

    @Override
    public Bolesnik registrirajBolesnika(Bolesnik bolesnik){
        Assert.notNull(bolesnik, "Mora biti objekt Bolesnik...");
        Assert.isNull(bolesnik,"");
        return bolesnikRepo.save(bolesnik);
    }

    @Override
    public List<Bolesnik> searchBolesniks(String query){
        List<Bolesnik> bolesnici = bolesnikRepo.searchBolesniks(query);
        return bolesnici;
    }

    public List<Bolesnik> searchBolesnikById(String query){
        List<Bolesnik> bolesnici = bolesnikRepo.searchBolesnikById(query);
        return bolesnici;
    }

    @Override
    public List<Bolesnik> poIdu(Long id){
        List<Bolesnik> bolesnici = bolesnikRepo.poIdu(id);
        return bolesnici;
    }
}
