package progi.fizikalna_terapija_v1.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import progi.fizikalna_terapija_v1.dao.KorisnikRepository;
import progi.fizikalna_terapija_v1.domain.Korisnik;
import progi.fizikalna_terapija_v1.service.KorisnikService;

@Service
public class KorisnikServiceImpl implements KorisnikService {

    @Autowired
    private KorisnikRepository korisnikRepository;

    @Override
    public Korisnik vratiKorisnika(String username){
        Korisnik korisnik = korisnikRepository.vratiKorisnika(username);
        return korisnik;
    }
}
