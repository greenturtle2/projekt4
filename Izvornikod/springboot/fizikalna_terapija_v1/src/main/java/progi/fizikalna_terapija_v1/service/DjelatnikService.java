package progi.fizikalna_terapija_v1.service;

import progi.fizikalna_terapija_v1.domain.Djelatnik;

import java.util.List;
import java.util.Optional;

public interface DjelatnikService {
    List<Djelatnik> listAll();

    Djelatnik registrirajDjelatnika(Djelatnik djelatnik);

    //#########################################

    List<Djelatnik> searchDjelatnikById(Long id);

    List<Djelatnik> searchDjelatnikByEmail(String email);

}
