package progi.fizikalna_terapija_v1.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import progi.fizikalna_terapija_v1.domain.Djelatnik;
import progi.fizikalna_terapija_v1.service.DjelatnikService;

import java.util.List;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/")
public class DjelatnikController {

    @Autowired
    private DjelatnikService djelatnikService;

    @GetMapping("/djelatnik")
    @PreAuthorize("hasRole('MEDIC')")
    public String djelatnikAccess(){
        return "Stranica DJELATNIK.";
    }

    @GetMapping("/djelatnici")
    public List<Djelatnik> listDjelatnici(){
        return djelatnikService.listAll();
    }

    @GetMapping("/djelatnik/{id}")
    public ResponseEntity<List<Djelatnik>> searchDjelatnikById(@PathVariable(value = "id") Long id){
        return ResponseEntity.ok(djelatnikService.searchDjelatnikById(id));
    }


}
