package progi.fizikalna_terapija_v1.payload.request;

import javax.persistence.Column;
import java.util.Set;

public class SignupRequestBolesnik {

    private Long id;

    private String ime;

    private String prezime;

    private String telefon;

    private String email;

    private String dijagnoza;

    private String mbo;

    private String dodatno_zdravstveno;

    private String moj_lijecnik;



    private Set<String> uloga;

    public String getIme() {
        return ime;
    }

    public void setIme(String ime) {
        this.ime = ime;
    }

    public String getPrezime() {
        return prezime;
    }

    public void setPrezime(String prezime) {
        this.prezime = prezime;
    }

    public String getTelefon() {
        return telefon;
    }

    public void setTelefon(String telefon) {
        this.telefon = telefon;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getDijagnoza() {
        return dijagnoza;
    }

    public void setDijagnoza(String dijagnoza) {
        this.dijagnoza = dijagnoza;
    }

    public String getMbo() {
        return mbo;
    }

    public void setMbo(String mbo) {
        this.mbo = mbo;
    }

    public String getDodatno_zdravstveno() {
        return dodatno_zdravstveno;
    }

    public void setDodatno_zdravstveno(String dodatno_zdravstveno) {
        this.dodatno_zdravstveno = dodatno_zdravstveno;
    }

    public String getMoj_lijecnik() {
        return moj_lijecnik;
    }

    public void setMoj_lijecnik(String moj_lijecnik) {
        this.moj_lijecnik = moj_lijecnik;
    }


    public Set<String> getUloga() {
        return uloga;
    }

    public void setUloga(Set<String> uloga) {
        this.uloga = uloga;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
