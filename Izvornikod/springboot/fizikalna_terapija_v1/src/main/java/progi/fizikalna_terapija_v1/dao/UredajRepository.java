package progi.fizikalna_terapija_v1.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import progi.fizikalna_terapija_v1.domain.Uredaj;

import java.util.List;

@Repository
public interface UredajRepository extends JpaRepository<Uredaj, Long> {

    /*@Query("SELECT * FROM uredaj JOIN bolesnik ON uredaj.uredaj_id = bolesnik.uredaj_id JOIN " +
            "termin ON termin.bolesnik_id = bolesnik.bolesnik_id WHERE pocetakTermina < CURRENT_TIMESTAMP AND krajTermina" +
            " > CURRENT_TIMESTAMP")
    List<Uredaj> listTakenDevices();*/

}
