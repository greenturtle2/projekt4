package progi.fizikalna_terapija_v1.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Zavrsetak {

    @Id
    @GeneratedValue
    private Long id;

    private String bolesnik;

    private String napredak;

    private String zadovoljan;

    private String izjava;

    private String zapazanje;

    public Zavrsetak() {
    }

    public Zavrsetak(String bolesnik, String napredak, String zadovoljan, String izjava, String zapazanje) {
        this.bolesnik = bolesnik;
        this.napredak = napredak;
        this.zadovoljan = zadovoljan;
        this.izjava = izjava;
        this.zapazanje = zapazanje;
    }

    public String getBolesnik() {
        return bolesnik;
    }

    public void setBolesnik(String bolesnik) {
        this.bolesnik = bolesnik;
    }

    public String getNapredak() {
        return napredak;
    }

    public void setNapredak(String napredak) {
        this.napredak = napredak;
    }

    public String getZadovoljan() {
        return zadovoljan;
    }

    public void setZadovoljan(String zadovoljan) {
        this.zadovoljan = zadovoljan;
    }

    public String getIzjava() {
        return izjava;
    }

    public void setIzjava(String izjava) {
        this.izjava = izjava;
    }

    public String getZapazanje() {
        return zapazanje;
    }

    public void setZapazanje(String zapazanje) {
        this.zapazanje = zapazanje;
    }

    @Override
    public String toString() {
        return "    Napredak pacijenta: " + napredak + "\n" +
                "     Je li pacijent zadovoljan: " + zadovoljan + "\n" +
                "     Izjava pacijenta: " + izjava + "\n" +
                "     Zapažanje liječnika: " + zapazanje + "\n";
    }
}
