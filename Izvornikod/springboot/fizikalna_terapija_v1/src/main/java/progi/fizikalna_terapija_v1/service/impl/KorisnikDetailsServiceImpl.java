package progi.fizikalna_terapija_v1.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import progi.fizikalna_terapija_v1.dao.KorisnikRepository;
import progi.fizikalna_terapija_v1.domain.Korisnik;

import javax.transaction.Transactional;

@Service
public class KorisnikDetailsServiceImpl implements UserDetailsService {

    @Autowired
    KorisnikRepository korisnikRepository;

    @Override
    @Transactional
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Korisnik korisnik = korisnikRepository.findByUsername(username)
                .orElseThrow(() -> new UsernameNotFoundException("Ne postoji korisnik sa tim username-om: " + username));
        return KorisnikDetailsImpl.build(korisnik);
    }
}
