package progi.fizikalna_terapija_v1.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import progi.fizikalna_terapija_v1.dao.ZavrsetakRepository;
import progi.fizikalna_terapija_v1.domain.Zavrsetak;
import progi.fizikalna_terapija_v1.dto.ZavrsetakDto;
import progi.fizikalna_terapija_v1.service.ZavrsetakService;

import java.util.List;

@Service
public class ZavrsetakServiceImpl implements ZavrsetakService {

    @Autowired
    private ZavrsetakRepository zavrsetakRepository;

    public String addZavrsetak(ZavrsetakDto zavrsetakDto){
        Zavrsetak zavrsetak = new Zavrsetak(zavrsetakDto.getBolesnik(), zavrsetakDto.getNapredak(),
                                            zavrsetakDto.getZadovoljan(), zavrsetakDto.getIzjava(),
                                            zavrsetakDto.getZapazanje());
        zavrsetakRepository.save(zavrsetak);
        return zavrsetak.getBolesnik();
    }

    @Override
    public List<Zavrsetak> searchZavrsetak(String query){
        List<Zavrsetak> zavrseci = zavrsetakRepository.searchZavrsetak(query);
        return zavrseci;
    }

}
