package progi.fizikalna_terapija_v1.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import progi.fizikalna_terapija_v1.domain.Evidencija;
import progi.fizikalna_terapija_v1.service.EvidencijaService;

import java.util.List;
import java.util.Optional;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/evidencije")
public class EvidencijaController {

    @Autowired
    private EvidencijaService evidencijaService;

    @GetMapping("")
    public List<Evidencija> listEvidencije(){
        return evidencijaService.listAll();
    }


    @GetMapping("/{id}")
    public ResponseEntity<List<Evidencija>> searchEvidencijas(@PathVariable(value = "id") String query){
        return ResponseEntity.ok(evidencijaService.searchEvidencijas(query));
    }

}
