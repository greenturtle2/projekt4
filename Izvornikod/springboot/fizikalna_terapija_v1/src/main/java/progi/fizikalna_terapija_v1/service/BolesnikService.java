package progi.fizikalna_terapija_v1.service;

import progi.fizikalna_terapija_v1.domain.Bolesnik;

import java.util.List;

public interface BolesnikService {

    List<Bolesnik> listAll();

    Bolesnik registrirajBolesnika(Bolesnik bolesnik);

    List<Bolesnik> searchBolesniks(String query);

    List<Bolesnik> searchBolesnikById(String query);

    List<Bolesnik> poIdu(Long id);
}
