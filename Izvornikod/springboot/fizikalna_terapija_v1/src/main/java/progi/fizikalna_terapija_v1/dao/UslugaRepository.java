package progi.fizikalna_terapija_v1.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import progi.fizikalna_terapija_v1.domain.Usluga;

import java.util.List;

@Repository
public interface UslugaRepository extends JpaRepository<Usluga, Long> {

    @Query("SELECT u FROM Usluga u WHERE u.naziv =?1")
    List<Usluga> vratiUslugu(String naziv);
}
