package progi.fizikalna_terapija_v1.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import progi.fizikalna_terapija_v1.domain.Usluga;
import progi.fizikalna_terapija_v1.service.UslugaService;

import java.util.List;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/usluge")
public class UslugaController2 {

    @Autowired
    private UslugaService uslugaService;

    @GetMapping("")
    public List<Usluga> listUsluge(){
        return uslugaService.listAll();
    }
}
