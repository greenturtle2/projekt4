package progi.fizikalna_terapija_v1.rest;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HomeController {

    @RequestMapping("/")
    public String ispisiPocetnu(){
        return "Pocetna";
    }
}
