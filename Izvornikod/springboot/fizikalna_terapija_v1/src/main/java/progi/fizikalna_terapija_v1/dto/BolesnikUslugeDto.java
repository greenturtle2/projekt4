package progi.fizikalna_terapija_v1.dto;

public class BolesnikUslugeDto {

    private Long id;

    private String bolesnik_id;

    private String usluga;

    public BolesnikUslugeDto() {
    }

    public BolesnikUslugeDto(Long id, String bolesnik_id, String usluga) {
        this.id = id;
        this.bolesnik_id = bolesnik_id;
        this.usluga = usluga;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getBolesnik_id() {
        return bolesnik_id;
    }

    public void setBolesnik_id(String bolesnik_id) {
        this.bolesnik_id = bolesnik_id;
    }

    public String getUsluga() {
        return usluga;
    }

    public void setUsluga(String usluga) {
        this.usluga = usluga;
    }
}
