package progi.fizikalna_terapija_v1.rest;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/")
public class VoditeljController {

    @GetMapping("/voditelj")
    @PreAuthorize("hasRole('VODITELJ')")
    public String voditeljAccess(){
        return "Stranica VODITELJ.";
    }
}
