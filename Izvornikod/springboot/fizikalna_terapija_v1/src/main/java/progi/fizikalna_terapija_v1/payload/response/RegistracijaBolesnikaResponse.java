package progi.fizikalna_terapija_v1.payload.response;

public class RegistracijaBolesnikaResponse {

    private String message;

    private String id;

    public RegistracijaBolesnikaResponse(String message, String id) {
        this.message = message;
        this.id = id;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
