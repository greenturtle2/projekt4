package progi.fizikalna_terapija_v1.dao;

import java.time.LocalDate;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import progi.fizikalna_terapija_v1.domain.DnevniVoditelj;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

@Repository
public interface DnevniVoditeljRepository extends JpaRepository<DnevniVoditelj, Long> {

    @Query("select a from DnevniVoditelj a where a.danVodenja = :danVodenja")
    List<DnevniVoditelj> findByDate(@Param("danVodenja") LocalDate danVodenja);



}
