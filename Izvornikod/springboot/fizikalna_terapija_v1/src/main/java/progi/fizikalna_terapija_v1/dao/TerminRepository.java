package progi.fizikalna_terapija_v1.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import progi.fizikalna_terapija_v1.domain.Termin;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

public interface TerminRepository extends JpaRepository<Termin, Long> {

    @Query("SELECT t FROM Termin t WHERE " +
            "t.bolesnikId LIKE CONCAT('%', :query, '%')")
    List<Termin> searchTerminsBy(String query);





}
