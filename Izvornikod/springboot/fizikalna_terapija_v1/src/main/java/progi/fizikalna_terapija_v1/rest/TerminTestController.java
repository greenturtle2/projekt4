package progi.fizikalna_terapija_v1.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.*;
import progi.fizikalna_terapija_v1.domain.Termin;
import progi.fizikalna_terapija_v1.domain.TerminTest;
import progi.fizikalna_terapija_v1.service.TerminTestService;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/terminitest")
public class TerminTestController {

    @Autowired
    private TerminTestService terminTestService;

    @GetMapping("")
    public List<TerminTest> showTerminTest(){
        List<TerminTest> terminTests = terminTestService.listAll();
        return terminTests;
    }

    @GetMapping("/podanu/{dan}")
    public List<TerminTest> poDanu(@PathVariable("dan") @DateTimeFormat(pattern = "dd-MM-yyyy")
                                   LocalDate dan){
        return terminTestService.poDanu(dan);
    }

    @GetMapping("/povri/{vri}")
    public List<TerminTest> poVri(@PathVariable("vri") @DateTimeFormat(pattern = "HH:mm")
                                  LocalTime vri){
        return terminTestService.poVri(vri);
    }

    @RequestMapping(value = "/danvri/{dan}/{vri}", method = RequestMethod.GET)
    public List<TerminTest> poDanuIVri(@PathVariable("dan") @DateTimeFormat(pattern = "dd-MM-yyyy")
                                       LocalDate dan,
                                       @PathVariable("vri") @DateTimeFormat(pattern = "HH:mm")
                                       LocalTime vri){
        return terminTestService.poDanuIVri(dan, vri);
    }

    @GetMapping("/pobolesniku/{bolesnik_id}")
    public List<String> poBolesniku(@PathVariable("bolesnik_id") String bolesnik_id){
        List<String> termini = new ArrayList<String>();

        List<TerminTest> terminTests = terminTestService.poBolesniku(bolesnik_id);

        for (TerminTest terminTest : terminTests){
            String jedanTermin = "";
            String usluga = terminTest.getUsluga();
            LocalDate dan = terminTest.getDan();
            LocalTime vrijeme = terminTest.getVrijeme();

            java.time.DayOfWeek danUTjednu;
            int redniBrojDana;
            for(int i = 0; i<10; i++){
                jedanTermin = "";
                danUTjednu = dan.getDayOfWeek();
                redniBrojDana = danUTjednu.getValue();
                if(redniBrojDana==6 || redniBrojDana==7){
                    i--;
                }else{
                    jedanTermin = jedanTermin + dan.toString();
                    jedanTermin = jedanTermin + " ";
                    jedanTermin = jedanTermin + vrijeme.toString();
                    jedanTermin = jedanTermin + " ";
                    jedanTermin = jedanTermin + usluga ;
                    termini.add(jedanTermin);
                }

                dan = dan.plusDays(1);


            }


        }


        return termini;
    }

}
