package progi.fizikalna_terapija_v1.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;
import progi.fizikalna_terapija_v1.dao.AdministratorRepository;
import progi.fizikalna_terapija_v1.dao.VoditeljRepository;
import progi.fizikalna_terapija_v1.domain.Administrator;
import progi.fizikalna_terapija_v1.domain.Voditelj;
import progi.fizikalna_terapija_v1.service.VoditeljService;

import java.util.List;

@Service
public class VoditeljServiceJpa implements VoditeljService {


    @Autowired
    private VoditeljRepository voditeljRepo;


    @Override
    public List<Voditelj> listAll(){
        return voditeljRepo.findAll();
    }


    @Override
    public Voditelj registrirajVoditelja(Voditelj voditelj){
        Assert.notNull(voditelj, "Mora biti objekt Voditelj...");
        Assert.isNull(voditelj.getId(), "");
        return voditeljRepo.save(voditelj);
    }

    public List<Voditelj> searchVoditeljByEmail(String email){
        List<Voditelj> voditelji = voditeljRepo.searchVoditeljByEmail(email);
        return voditelji;
    }
}
