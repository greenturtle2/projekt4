package progi.fizikalna_terapija_v1.dto;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.time.LocalDateTime;

public class TerminDto {

    private Long terminId;

    @JsonFormat(pattern = "yyyy-MM-dd-HH-mm-ss")
    private LocalDateTime pocetakTermina;

    @JsonFormat(pattern = "yyyy-MM-dd-HH-mm-ss")
    private LocalDateTime krajTermina;

    private String bolesnikId;

    private String naziv_usluga;

    private Integer usluga_trajanje;

    public TerminDto() {}

    public TerminDto(Long terminId, LocalDateTime pocetakTermina, LocalDateTime krajTermina, String bolesnikId,
                     String naziv_usluga, Integer usluga_trajanje) {
        this.terminId = terminId;
        this.pocetakTermina = pocetakTermina;
        this.krajTermina = krajTermina;
        this.bolesnikId = bolesnikId;
        this.naziv_usluga = naziv_usluga;
        this.usluga_trajanje = usluga_trajanje;
    }

    public Long getTerminId() {
        return terminId;
    }

    public void setTerminId(Long terminId) {
        this.terminId = terminId;
    }

    @JsonFormat(pattern = "yyyy-MM-dd-HH-mm-ss")
    public LocalDateTime getPocetakTermina() {
        return pocetakTermina;
    }

    @JsonFormat(pattern = "yyyy-MM-dd-HH-mm-ss")
    public void setPocetakTermina(LocalDateTime pocetakTermina) {
        this.pocetakTermina = pocetakTermina;
    }

    @JsonFormat(pattern = "yyyy-MM-dd-HH-mm-ss")
    public LocalDateTime getKrajTermina() {
        return krajTermina;
    }

    @JsonFormat(pattern = "yyyy-MM-dd-HH-mm-ss")
    public void setKrajTermina(LocalDateTime krajTermina) {
        this.krajTermina = krajTermina;
    }

    public String getBolesnikId() {
        return bolesnikId;
    }

    public void setBolesnikId(String bolesnikId) {
        this.bolesnikId = bolesnikId;
    }

    public String getNaziv_usluga() {
        return naziv_usluga;
    }

    public void setNaziv_usluga(String naziv_usluga) {
        this.naziv_usluga = naziv_usluga;
    }

    public Integer getUsluga_trajanje() {
        return usluga_trajanje;
    }

    public void setUsluga_trajanje(Integer usluga_trajanje) {
        this.usluga_trajanje = usluga_trajanje;
    }
}
