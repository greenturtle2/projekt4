package progi.fizikalna_terapija_v1.service;

import progi.fizikalna_terapija_v1.domain.Administrator;

import java.util.List;

public interface AdministratorService {

    List<Administrator> listAll();

    Administrator registrirajAdministratora(Administrator administrator);

    List<Administrator> searchAdministratorByEmail(String email);
}
