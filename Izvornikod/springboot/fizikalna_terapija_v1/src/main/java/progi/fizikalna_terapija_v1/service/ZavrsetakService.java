package progi.fizikalna_terapija_v1.service;

import progi.fizikalna_terapija_v1.domain.Zavrsetak;
import progi.fizikalna_terapija_v1.dto.ZavrsetakDto;

import java.util.List;

public interface ZavrsetakService {

    String addZavrsetak(ZavrsetakDto zavrsetakDto);

    List<Zavrsetak> searchZavrsetak(String query);

}
