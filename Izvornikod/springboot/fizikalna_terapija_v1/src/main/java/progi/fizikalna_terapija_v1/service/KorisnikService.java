package progi.fizikalna_terapija_v1.service;

import progi.fizikalna_terapija_v1.domain.Korisnik;

public interface KorisnikService {

    Korisnik vratiKorisnika(String username);

}
