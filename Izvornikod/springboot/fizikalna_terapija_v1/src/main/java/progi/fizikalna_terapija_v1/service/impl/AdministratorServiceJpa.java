package progi.fizikalna_terapija_v1.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;
import progi.fizikalna_terapija_v1.dao.AdministratorRepository;
import progi.fizikalna_terapija_v1.domain.Administrator;
import progi.fizikalna_terapija_v1.domain.Djelatnik;
import progi.fizikalna_terapija_v1.service.AdministratorService;

import java.util.List;

@Service
public class AdministratorServiceJpa implements AdministratorService {

    @Autowired
    private AdministratorRepository administratorRepo;

    @Override
    public List<Administrator> listAll(){
        return administratorRepo.findAll();
    }

    @Override
    public Administrator registrirajAdministratora(Administrator administrator){
        Assert.notNull(administrator, "Mora biti objekt Administrator...");
        Assert.isNull(administrator.getId(),"");
        return administratorRepo.save(administrator);
    }

    public List<Administrator> searchAdministratorByEmail(String email){
        List<Administrator> administratori = administratorRepo.searchAdministratorByEmail(email);
        return administratori;
    }
}
