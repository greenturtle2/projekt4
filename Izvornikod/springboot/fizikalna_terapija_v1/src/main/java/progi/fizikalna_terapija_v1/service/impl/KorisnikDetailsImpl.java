package progi.fizikalna_terapija_v1.service.impl;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import progi.fizikalna_terapija_v1.domain.Korisnik;

import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class KorisnikDetailsImpl implements UserDetails {
    private static final long serialVersionUID = 1L;

    private Long id;
    private String username;

    @JsonIgnore
    private String password;

    private Collection<? extends GrantedAuthority> authorities;

    private String ime;

    public KorisnikDetailsImpl(Long id, String username, String password, Collection<? extends GrantedAuthority> authorities, String ime) {
        this.id = id;
        this.username = username;
        this.password = password;
        this.authorities = authorities;
        this.ime = ime;
    }

    public static KorisnikDetailsImpl build(Korisnik korisnik){
        List<GrantedAuthority> authorities = korisnik.getUloge().stream()
                .map(uloga -> new SimpleGrantedAuthority(uloga.getNaziv().name()))
                .collect(Collectors.toList());

        return new KorisnikDetailsImpl(
                korisnik.getId(),
                korisnik.getUsername(),
                korisnik.getPassword(),
                authorities,
                korisnik.getIme());
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities(){return authorities;}

    public Long getId(){return id;}

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public String getUsername() {
        return username;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    @Override
    public boolean equals(Object o) {
        if(this == o)
            return true;
        if(o == null || getClass() != o.getClass())
            return false;
        KorisnikDetailsImpl korisnik = (KorisnikDetailsImpl) o;
        return Objects.equals(id, korisnik.id);
    }

    public String getIme() {
        return ime;
    }

    public void setIme(String ime) {
        this.ime = ime;
    }
}
