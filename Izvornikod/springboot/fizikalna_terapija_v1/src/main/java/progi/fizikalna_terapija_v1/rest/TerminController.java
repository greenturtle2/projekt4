package progi.fizikalna_terapija_v1.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import progi.fizikalna_terapija_v1.domain.Termin;
import progi.fizikalna_terapija_v1.dto.TerminDto;
import progi.fizikalna_terapija_v1.service.TerminService;

import java.time.LocalDateTime;
import java.util.List;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/termini")
public class TerminController {

    @Autowired
    private TerminService terminService;

    @GetMapping("")
    public List<Termin> showTermins() {
        List<Termin> termini = terminService.listAll();
        return termini;
    }

    /*@PostMapping("/delete")
    public void deleteTermin(@RequestBody TerminDto terminDto) {
        terminService.removeTermin(terminDto);
        return;
    }*/

    @GetMapping("/{id}")
    public ResponseEntity<List<Termin>> searchTermins(@PathVariable(value = "id") String query){
        return ResponseEntity.ok(terminService.searchTermins(query));
    }



}
