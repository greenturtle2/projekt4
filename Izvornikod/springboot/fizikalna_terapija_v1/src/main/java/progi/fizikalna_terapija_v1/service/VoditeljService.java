package progi.fizikalna_terapija_v1.service;

import progi.fizikalna_terapija_v1.domain.Administrator;
import progi.fizikalna_terapija_v1.domain.Voditelj;

import java.util.List;

public interface VoditeljService {

    List<Voditelj> listAll();

    Voditelj registrirajVoditelja(Voditelj voditelj);

    List<Voditelj> searchVoditeljByEmail(String email);

}
