package progi.fizikalna_terapija_v1.domain;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Uredaj {

    @Id
    @GeneratedValue
    @Column(name = "uredaj_id")
    private long uredajId;

    @Column(name = "naziv_uredaja")
    private String nazivUredaja;

    @Column(name = "slika_uredaja")
    private String slikaUredaja;

    public Uredaj(){}
    public Uredaj(String nazivUredaja, String slikaUredaja) {
        this.nazivUredaja = nazivUredaja;
        this.slikaUredaja = slikaUredaja;
    }

    public long getUredajId() {
        return uredajId;
    }

    public void setUredajId(long uredajId) {
        this.uredajId = uredajId;
    }

    public String getNazivUredaja() {
        return nazivUredaja;
    }

    public void setNazivUredaja(String nazivUredaja) {
        this.nazivUredaja = nazivUredaja;
    }

    public String getSlikaUredaja() {
        return slikaUredaja;
    }

    public void setSlikaUredaja(String slikaUredaja) {
        this.slikaUredaja = slikaUredaja;
    }
}
