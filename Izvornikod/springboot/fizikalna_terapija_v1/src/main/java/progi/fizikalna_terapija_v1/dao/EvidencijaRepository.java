package progi.fizikalna_terapija_v1.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import progi.fizikalna_terapija_v1.domain.Evidencija;

import java.util.List;
import java.util.Optional;

@Repository
public interface EvidencijaRepository extends JpaRepository<Evidencija, Long> {

    @Query("SELECT e FROM Evidencija e WHERE " +
            "e.bolesnik_id LIKE CONCAT('%', :query, '%')")
    List<Evidencija> searchEvidencijas(String query);


}
