package progi.fizikalna_terapija_v1.exception;

import org.springframework.http.HttpStatus;

public class ApiException {

    private final String msg;
    private final HttpStatus status;

    public ApiException(String msg, HttpStatus status) {
        this.msg = msg;
        this.status = status;
    }

    public String getMsg() {
        return msg;
    }

    public HttpStatus getStatus() {
        return status;
    }
}
