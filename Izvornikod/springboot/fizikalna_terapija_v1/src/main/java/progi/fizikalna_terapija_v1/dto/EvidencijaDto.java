package progi.fizikalna_terapija_v1.dto;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.Date;

public class EvidencijaDto {

    private Long id;

    private Date dan;

    private String bolesnik_id;

    private String doktor_id;

    public EvidencijaDto() {
    }

    public EvidencijaDto(Long id, Date dan, String bolesnik_id) {
        this.id = id;
        this.dan = dan;
        this.bolesnik_id = bolesnik_id;
    }

    public EvidencijaDto(Long id, Date dan, String bolesnik_id, String doktor_id) {
        this.id = id;
        this.dan = dan;
        this.bolesnik_id = bolesnik_id;
        this.doktor_id = doktor_id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @JsonFormat(pattern = "yyyy-MM-dd")
    public Date getDan() {
        return dan;
    }

    @JsonFormat(pattern = "yyyy-MM-dd")
    public void setDan(Date dan) {
        this.dan = dan;
    }

    public String getBolesnik_id() {
        return bolesnik_id;
    }

    public void setBolesnik_id(String bolesnik_id) {
        this.bolesnik_id = bolesnik_id;
    }

    public String getDoktor_id() {
        return doktor_id;
    }

    public void setDoktor_id(String doktor_id) {
        this.doktor_id = doktor_id;
    }
}
