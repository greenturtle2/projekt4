package progi.fizikalna_terapija_v1.service;

import progi.fizikalna_terapija_v1.domain.Evidencija;
import progi.fizikalna_terapija_v1.dto.EvidencijaDto;

import java.util.List;
import java.util.Optional;

public interface EvidencijaService {


    String addEvidencija(EvidencijaDto evidencijaDto);

    List<Evidencija> listAll();

    List<Evidencija> searchEvidencijas(String query);

}
