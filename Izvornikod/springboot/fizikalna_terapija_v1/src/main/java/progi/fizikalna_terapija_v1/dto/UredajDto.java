package progi.fizikalna_terapija_v1.dto;

public class UredajDto {

    private long uredajId;

    private String nazivUredaja;

    private String slikaUredaja;

    public UredajDto(){}

    public UredajDto(long uredajId, String nazivUredaja, String slikaUredaja) {
        this.uredajId = uredajId;
        this.nazivUredaja = nazivUredaja;
        this.slikaUredaja = slikaUredaja;
    }

    public long getUredajId() {
        return uredajId;
    }

    public void setUredajId(long uredajId) {
        this.uredajId = uredajId;
    }

    public String getNazivUredaja() {
        return nazivUredaja;
    }

    public void setNazivUredaja(String nazivUredaja) {
        this.nazivUredaja = nazivUredaja;
    }

    public String getSlikaUredaja() {
        return slikaUredaja;
    }

    public void setSlikaUredaja(String slikaUredaja) {
        this.slikaUredaja = slikaUredaja;
    }
}
