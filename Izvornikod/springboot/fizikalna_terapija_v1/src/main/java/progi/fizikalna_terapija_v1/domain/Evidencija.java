package progi.fizikalna_terapija_v1.domain;

import com.fasterxml.jackson.annotation.JsonFormat;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.util.Date;

@Entity
public class Evidencija {

    @Id
    @GeneratedValue
    private Long id;

    private Date dan;

    private String bolesnik_id;

    private String doktor_id;

    public Evidencija() {
    }

    public Evidencija(Date dan, String bolesnik_id) {
        this.dan = dan;
        this.bolesnik_id = bolesnik_id;
    }

    public Evidencija(Date dan, String bolesnik_id, String doktor_id) {
        this.dan = dan;
        this.bolesnik_id = bolesnik_id;
        this.doktor_id = doktor_id;
    }

    @JsonFormat(pattern = "yyyy-MM-dd")
    public Date getDan() {
        return dan;
    }

    @JsonFormat(pattern = "yyyy-MM-dd")
    public void setDan(Date dan) {
        this.dan = dan;
    }

    public String getBolesnik_id() {
        return bolesnik_id;
    }

    public void setBolesnik_id(String bolesnik_id) {
        this.bolesnik_id = bolesnik_id;
    }

    public String getDoktor_id() {
        return doktor_id;
    }

    public void setDoktor_id(String doktor_id) {
        this.doktor_id = doktor_id;
    }
}
