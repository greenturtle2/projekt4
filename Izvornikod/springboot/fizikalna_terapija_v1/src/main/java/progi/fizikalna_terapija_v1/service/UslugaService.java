package progi.fizikalna_terapija_v1.service;

import progi.fizikalna_terapija_v1.domain.Usluga;
import progi.fizikalna_terapija_v1.dto.UslugaDto;

import java.util.List;

public interface UslugaService {

    String addUsluga(UslugaDto uslugaDto);

    List<Usluga> listAll();

    List<Usluga> vratiUslugu(String naziv);

}
