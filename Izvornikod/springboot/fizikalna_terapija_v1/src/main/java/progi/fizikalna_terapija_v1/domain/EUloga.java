package progi.fizikalna_terapija_v1.domain;

public enum EUloga {
    ROLE_ADMIN,
    ROLE_VODITELJ,
    ROLE_MEDIC,
    ROLE_BOLESNIK
}
