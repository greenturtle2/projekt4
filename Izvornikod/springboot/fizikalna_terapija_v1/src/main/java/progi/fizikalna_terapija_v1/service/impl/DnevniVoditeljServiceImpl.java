package progi.fizikalna_terapija_v1.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import net.bytebuddy.asm.Advice.Local;
import progi.fizikalna_terapija_v1.dao.DnevniVoditeljRepository;
import progi.fizikalna_terapija_v1.domain.DnevniVoditelj;
import progi.fizikalna_terapija_v1.dto.DnevniVoditeljDto;
import progi.fizikalna_terapija_v1.service.DnevniVoditeljService;

import java.time.LocalDate;
import java.util.List;

@Service
public class DnevniVoditeljServiceImpl implements DnevniVoditeljService {

    @Autowired
    private DnevniVoditeljRepository dnevniVoditeljRepository;

    public List<DnevniVoditelj> listAll(){
        return dnevniVoditeljRepository.findAll();
    }

    @Override
    public List<DnevniVoditelj> getDnevniVoditeljByDate(LocalDate danVodenja) {
        return dnevniVoditeljRepository.findByDate(danVodenja);
    }

    public String addDnevniVoditelj(DnevniVoditeljDto dnevniVoditeljDto){
        DnevniVoditelj dnevniVoditelj = new DnevniVoditelj(dnevniVoditeljDto.getDanVodenja(),
                                                            dnevniVoditeljDto.getDoktor_id());
        dnevniVoditeljRepository.save(dnevniVoditelj);
        return dnevniVoditelj.getDoktor_id();
    }
}
