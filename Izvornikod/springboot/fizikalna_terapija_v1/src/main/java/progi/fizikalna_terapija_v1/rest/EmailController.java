package progi.fizikalna_terapija_v1.rest;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import progi.fizikalna_terapija_v1.domain.Korisnik;
import progi.fizikalna_terapija_v1.dto.EmailDto;
import progi.fizikalna_terapija_v1.payload.response.MessageResponse;
import progi.fizikalna_terapija_v1.service.KorisnikService;
import progi.fizikalna_terapija_v1.service.impl.EmailService;

@RestController
@CrossOrigin
@RequestMapping("/send")
public class EmailController {

    @Autowired
    private EmailService emailService;

    @Autowired
    private KorisnikService korisnikService;

    @PostMapping("/email")
    public ResponseEntity<?> sendEmail(@RequestBody EmailDto emailDto){
        emailService.sendEmail(emailDto.getPrimatelj(), emailDto.getPredmet(), emailDto.getPoruka());
        return ResponseEntity.ok(new MessageResponse("Email uspjesno poslan!"));
    }

    /*
    @PostMapping("/forgpass")
    public void forgPass(@RequestBody EmailDto emailDto){
        String username = emailDto.getPrimatelj();
        Korisnik korisnik = korisnikService.vratiKorisnika(username);
        System.out.println(korisnik.getPassword());
    } */
}
