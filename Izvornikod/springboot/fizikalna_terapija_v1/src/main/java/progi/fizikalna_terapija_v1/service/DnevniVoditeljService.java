package progi.fizikalna_terapija_v1.service;

import progi.fizikalna_terapija_v1.domain.DnevniVoditelj;
import progi.fizikalna_terapija_v1.dto.DnevniVoditeljDto;

import java.time.LocalDate;
import java.util.List;

public interface DnevniVoditeljService {

    List<DnevniVoditelj> listAll();

    List<DnevniVoditelj> getDnevniVoditeljByDate(LocalDate danVodenja);

    String addDnevniVoditelj(DnevniVoditeljDto dnevniVoditeljDto);
}