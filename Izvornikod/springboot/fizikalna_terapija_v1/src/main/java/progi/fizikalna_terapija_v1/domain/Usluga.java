package progi.fizikalna_terapija_v1.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.awt.*;

@Entity
public class Usluga {

    @Id
    @GeneratedValue
    private Long id;

    private String naziv;

    private String opis;

    private String slika;

    private int vrijeme;

    private int paralelno;

    public Usluga() {
    }

    public Usluga(String naziv, String opis, String slika, int vrijeme, int paralelno) {
        this.naziv = naziv;
        this.opis = opis;
        this.slika = slika;
        this.vrijeme = vrijeme;
        this.paralelno = paralelno;
    }

    public String getNaziv() {
        return naziv;
    }

    public void setNaziv(String naziv) {
        this.naziv = naziv;
    }

    public String getOpis() {
        return opis;
    }

    public void setOpis(String opis) {
        this.opis = opis;
    }

    public String getSlika() {
        return slika;
    }

    public void setSlika(String slika) {
        this.slika = slika;
    }

    public int getVrijeme() {
        return vrijeme;
    }

    public void setVrijeme(int vrijeme) {
        this.vrijeme = vrijeme;
    }

    public int getParalelno() {
        return paralelno;
    }

    public void setParalelno(int paralelno) {
        this.paralelno = paralelno;
    }
}
