package progi.fizikalna_terapija_v1.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;
import progi.fizikalna_terapija_v1.dao.DjelatnikRepository;
import progi.fizikalna_terapija_v1.domain.Djelatnik;
import progi.fizikalna_terapija_v1.service.DjelatnikService;

import java.util.List;
import java.util.Optional;

@Service
public class DjelatnikServiceJpa implements DjelatnikService {

    @Autowired
    private DjelatnikRepository djelatnikRepo;

    @Override
    public List<Djelatnik> listAll(){
        return djelatnikRepo.findAll();
    }

    @Override
    public Djelatnik registrirajDjelatnika(Djelatnik djelatnik){
        Assert.notNull(djelatnik, "Mora biti objekt Djelatnik...");
        Assert.isNull(djelatnik.getId(), "");
        return djelatnikRepo.save(djelatnik);
    }

    //#############################################

    public List<Djelatnik> searchDjelatnikById(Long id){
        List<Djelatnik> djelatnici = djelatnikRepo.searchDjelatnikById(id);
        return djelatnici;
    }

    public List<Djelatnik> searchDjelatnikByEmail(String email){
        List<Djelatnik> djelatnici = djelatnikRepo.searchDjelatnikByEmail(email);
        return djelatnici;
    }

}
