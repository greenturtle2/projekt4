package progi.fizikalna_terapija_v1.service;

import progi.fizikalna_terapija_v1.domain.TerminTest;
import progi.fizikalna_terapija_v1.dto.TerminTestDto;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;

public interface TerminTestService {

    List<TerminTest> listAll();

    void addTerminTest(TerminTestDto terminTestDto);

    List<TerminTest> poDanu(LocalDate dan);

    List<TerminTest> poVri(LocalTime vri);

    List<TerminTest> poDanuIVri(LocalDate dan, LocalTime vri);

    List<TerminTest> preklapanje(LocalDate dan, LocalTime poc, LocalTime kra);

    List<TerminTest> poUsluziIVri(String usluga, LocalTime poc, LocalTime kra);

    List<TerminTest> poBolesniku(String bolesnik_id);

    List<TerminTest> poUsluzi(String usluga);


}
