package progi.fizikalna_terapija_v1.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import progi.fizikalna_terapija_v1.dao.EvidencijaRepository;
import progi.fizikalna_terapija_v1.domain.Evidencija;
import progi.fizikalna_terapija_v1.dto.EvidencijaDto;
import progi.fizikalna_terapija_v1.service.EvidencijaService;

import java.util.List;
import java.util.Optional;

@Service
public class EvidencijaServiceImpl implements EvidencijaService {

    @Autowired
    private EvidencijaRepository evidencijaRepository;

    public String addEvidencija(EvidencijaDto evidencijaDto){
        Evidencija evidencija = new Evidencija(evidencijaDto.getDan(), evidencijaDto.getBolesnik_id(),
                evidencijaDto.getDoktor_id());
        evidencijaRepository.save(evidencija);
        return evidencija.getBolesnik_id();
    }

    public List<Evidencija> listAll(){
        return evidencijaRepository.findAll();
    }

    @Override
    public List<Evidencija> searchEvidencijas(String query){
        List<Evidencija> evidencije = evidencijaRepository.searchEvidencijas(query);
        return evidencije;
    }


}
