package progi.fizikalna_terapija_v1.dto;

public class UslugaDto {


    private Long id;

    private String naziv;

    private String opis;

    private String slika;

    private int vrijeme;

    private int paralelno;

    public UslugaDto() {
    }

    public UslugaDto(Long id, String naziv, String opis, String slika, int vrijeme, int paralelno) {
        this.id = id;
        this.naziv = naziv;
        this.opis = opis;
        this.slika = slika;
        this.vrijeme = vrijeme;
        this.paralelno = paralelno;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNaziv() {
        return naziv;
    }

    public void setNaziv(String naziv) {
        this.naziv = naziv;
    }

    public String getOpis() {
        return opis;
    }

    public void setOpis(String opis) {
        this.opis = opis;
    }

    public String getSlika() {
        return slika;
    }

    public void setSlika(String slika) {
        this.slika = slika;
    }

    public int getVrijeme() {
        return vrijeme;
    }

    public void setVrijeme(int vrijeme) {
        this.vrijeme = vrijeme;
    }

    public int getParalelno() {
        return paralelno;
    }

    public void setParalelno(int paralelno) {
        this.paralelno = paralelno;
    }
}
