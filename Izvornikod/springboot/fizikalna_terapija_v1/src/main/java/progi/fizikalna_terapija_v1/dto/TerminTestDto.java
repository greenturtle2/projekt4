package progi.fizikalna_terapija_v1.dto;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.time.LocalDate;
import java.time.LocalTime;

public class TerminTestDto {

    private Long id;

    @JsonFormat(pattern = "dd-MM-yyyy")
    private LocalDate dan;

    @JsonFormat(pattern = "HH:mm")
    private LocalTime vrijeme;

    private String bolesnik_id;

    private String usluga;

    public TerminTestDto() {
    }

    public TerminTestDto(Long id, LocalDate dan, LocalTime vrijeme, String bolesnik_id, String usluga) {
        this.id = id;
        this.dan = dan;
        this.vrijeme = vrijeme;
        this.bolesnik_id = bolesnik_id;
        this.usluga = usluga;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @JsonFormat(pattern = "dd-MM-yyyy")
    public LocalDate getDan() {
        return dan;
    }

    @JsonFormat(pattern = "dd-MM-yyyy")
    public void setDan(LocalDate dan) {
        this.dan = dan;
    }

    @JsonFormat(pattern = "HH:mm")
    public LocalTime getVrijeme() {
        return vrijeme;
    }

    @JsonFormat(pattern = "HH:mm")
    public void setVrijeme(LocalTime vrijeme) {
        this.vrijeme = vrijeme;
    }

    public String getBolesnik_id() {
        return bolesnik_id;
    }

    public void setBolesnik_id(String bolesnik_id) {
        this.bolesnik_id = bolesnik_id;
    }

    public String getUsluga() {
        return usluga;
    }

    public void setUsluga(String usluga) {
        this.usluga = usluga;
    }
}
