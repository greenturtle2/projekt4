package progi.fizikalna_terapija_v1.service;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import progi.fizikalna_terapija_v1.domain.Termin;
import progi.fizikalna_terapija_v1.dto.TerminDto;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.List;

public interface TerminService {

    List<Termin> listAll();

    void addTermin(TerminDto terminDto);

    void removeTermin(TerminDto terminDto);

    List<Termin> searchTermins(String query);


}
